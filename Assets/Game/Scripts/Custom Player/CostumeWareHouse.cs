﻿using System.Collections.Generic;
using UnityEngine;

public class CostumeWareHouse : MonoBehaviour
{
    [SerializeField] private CustomBank _customBank;
    private Dictionary<string, List<CustomDetails>> _kindOfCostumes = new Dictionary<string, List<CustomDetails>>();

    private void Awake()
    {
        _kindOfCostumes = new Dictionary<string, List<CustomDetails>>(_customBank.GetCustomsDetails());
    }

    public List<CustomDetails> GetCostumes(string idKindOfCostume)
    {
        if (_kindOfCostumes.ContainsKey(idKindOfCostume))
        {
            return _kindOfCostumes[idKindOfCostume];
        }
        return new List<CustomDetails>();
    }
}