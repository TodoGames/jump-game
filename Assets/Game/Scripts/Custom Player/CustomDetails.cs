﻿using UnityEngine;
using System;

[Serializable]
public struct CustomDetails
{
    public string _idCustom;
    public string _nameCustom;
    public Sprite _spriteImage;
    public GameObject _prefabObject;
}