﻿using System.Collections.Generic;
using System;

[Serializable]
public struct KindOfCustoms
{
    public string _titleCustom;
    public List<CustomDetails> _customsDetails;
}
