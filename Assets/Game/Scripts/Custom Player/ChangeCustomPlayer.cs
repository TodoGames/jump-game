using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using PlayFab.ClientModels;

public class ChangeCustomPlayer : MonoBehaviour
{
    [SerializeField] private CustomBank _customBank;
    [SerializeField] private List<ChangeCustom> _changeCustoms = new List<ChangeCustom>();
    private Dictionary<string, ChangeCustom> _currentChangeCustoms = new Dictionary<string, ChangeCustom>();

    private void Start()
    {
        foreach (var item in _changeCustoms)
        {
            _currentChangeCustoms.Add(item._idKindOfCustom, item);
        }
    }

    public void UpdateCustoms()
    {
        foreach (var item in _currentChangeCustoms)
        {
            item.Value.OnClickChangeCustom();
        }
    }

    public void SetIdConfiguration(string idKindOfCustom,string idCustomDetail)
    {
        if (_currentChangeCustoms.ContainsKey(idKindOfCustom))
        {
            _currentChangeCustoms[idKindOfCustom]._idKindOfCustom = idCustomDetail;
        }
    }

    public void LoadPlayerCustomSetting()
    {
        PlayfabPlayerData playfabPlayerData = new PlayfabPlayerData();
        playfabPlayerData.onGetUserDataSuccess += UdatePlayer;
        playfabPlayerData.GetPlayerData(PlayerPrefs.GetString("ID", ""));
    }

    private void UdatePlayer(GetUserDataResult obj)
    {
        if (obj.Data.ContainsKey("CustomPlayer"))
        {
            CustomSetting customSetting = new CustomSetting();
            customSetting = JsonUtility.FromJson<CustomSetting>(obj.Data["CustomPlayer"].Value);
            foreach (var item in customSetting._customObjects)
            {
                if (_currentChangeCustoms.ContainsKey(item._idCustomSetting))
                {
                    CustomDetails currentDetail = _customBank.GetCustomDetailByIds(item._idCustomSetting, item._idCustomObject);
                    _currentChangeCustoms[item._idCustomSetting].SetSpriteToChange(currentDetail._spriteImage);
                    _currentChangeCustoms[item._idCustomSetting].SetPrefabToChange(currentDetail._prefabObject);
                    _currentChangeCustoms[item._idCustomSetting].OnClickChangeCustom();
                }
            }
        }
    }
}

[Serializable]
public struct CustomSetting
{
    public List<CustomObject> _customObjects;

    public void AddNewCustom(string idCustomSetting, string idCustomObject)
    {
        if(_customObjects == null)
        {
            _customObjects = new List<CustomObject>();
        }

        int index = 0;
        if (IsOccupedName(idCustomSetting, out index))
        {
            _customObjects[index].SetIdCustomObject(idCustomObject);
        }
        else
        {
            CustomObject newCustomSetting = new CustomObject(idCustomSetting, idCustomObject);
            _customObjects.Add(newCustomSetting);
        }
    }

    private bool IsOccupedName(string idCustomSetting, out int index)
    {
        index = 0;
        for (int i = 0; i < _customObjects.Count; i++)
        {
            if(_customObjects[i]._idCustomSetting == idCustomSetting)
            {
                index = i;
                return true;
            }
        }
        return false;
    }

    [Serializable]
    public struct CustomObject
    {
        public string _idCustomSetting;
        public string _idCustomObject;

        public CustomObject(string idCustomSetting, string idCustomObject)
        {
            _idCustomSetting = idCustomSetting;
            _idCustomObject = idCustomObject;
        }

        public void SetIdCustomObject(string idCustomObject)
        {
            _idCustomObject = idCustomObject;
        }
    }
}