using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomSelection : MonoBehaviour
{
    [SerializeField] private CustomBank _customBank;
    [SerializeField] private Transform _parentBtnsKindOfCustoms;
    [SerializeField] private Button _btnKindOfCustom;
    [SerializeField] private Transform _parentPanelItems;
    [SerializeField] private Transform _panelItems;
    [SerializeField] private CustomSelectionItem _customSelectionItem;
    //[SerializeField] private ChangeCustomPlayer _customPlayer;
    private Dictionary<Button, GameObject> _currentPanels = new Dictionary<Button, GameObject>();

    private string _keyCustomSetting = "CustomPlayer";
    private CustomSetting _customSetting;

    private void Start()
    {
        _customSetting = new CustomSetting();

        foreach (var item in _customBank.GetCustoms())
        {
            Button newButtonKindOfCustom = Instantiate(_btnKindOfCustom, _parentBtnsKindOfCustoms);
            newButtonKindOfCustom.GetComponentInChildren<Text>().text = item._titleCustom;
            newButtonKindOfCustom.onClick.AddListener(() => { OnClickNextItems(newButtonKindOfCustom); });

            Transform newPanelItems = Instantiate(_panelItems, _parentPanelItems);
            foreach (var selectionItems in item._customsDetails)
            {
                CustomSelectionItem customSelectionItem = Instantiate(_customSelectionItem, newPanelItems);
                customSelectionItem.ConfigureButton(selectionItems._spriteImage, item._titleCustom,selectionItems._idCustom, this);
            }
            newPanelItems.gameObject.SetActive(false);
            _currentPanels.Add(newButtonKindOfCustom, newPanelItems.gameObject);
        }

        foreach (var item in _currentPanels)
        {
            OnClickNextItems(item.Key);
            break;
        }
    }

    public void OnClickNextItems(Button currentButtonClick)
    {
        foreach (var item in _currentPanels)
        {
            if(currentButtonClick == item.Key)
            {
                item.Value.SetActive(true);
                item.Key.interactable = false;
            }
            else
            {
                item.Value.SetActive(false);
                item.Key.interactable = true;
            }
        }
    }

    public void OnClickSelectItem(string idKindOfCustom,string idCustomDetail)
    {
        //_customPlayer.SetIdConfiguration(idKindOfCustom, idCustomDetail);
        _customSetting.AddNewCustom(idKindOfCustom, idCustomDetail);
    }

    public void OnClickSaveCustomSetting()
    {
        PlayfabPlayerData playfabPlayerData = new PlayfabPlayerData();
        string jsonSetting = JsonUtility.ToJson(_customSetting);
        playfabPlayerData.SetPlayerTileData(new Dictionary<string, string>() { { _keyCustomSetting, jsonSetting } });
    }
}
