using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Costume Bank", menuName = "Costume Configuration/Costume Bank")]
public class CustomBank : ScriptableObject
{
    [SerializeField] private List<KindOfCustoms> _Customs = new List<KindOfCustoms>();

    public List<KindOfCustoms> GetCustoms()
    {
        return _Customs;
    }

    public Dictionary<string, List<CustomDetails>> GetCustomsDetails()
    {
        Dictionary<string, List<CustomDetails>> currentCustoms = new Dictionary<string, List<CustomDetails>>();
        foreach (var item in _Customs)
        {
            currentCustoms.Add(item._titleCustom, item._customsDetails);
        }
        return currentCustoms;
    }

    public CustomDetails GetCustomDetailByIds(string idCustom, string idObject)
    {
        foreach (var item in _Customs)
        {
            if(item._titleCustom == idCustom)
            {
                foreach (var currentObjects in item._customsDetails)
                {
                    if(currentObjects._idCustom == idObject)
                    {
                        return currentObjects;
                    }
                }
            }
        }
        return new CustomDetails();
    }

    public void SetNewCustoms(List<KindOfCustoms> newListCustoms)
    {
        _Customs = new List<KindOfCustoms>(newListCustoms);
    }
}
