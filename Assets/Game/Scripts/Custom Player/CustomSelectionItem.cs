﻿using UnityEngine;
using UnityEngine.UI;

public class CustomSelectionItem : MonoBehaviour
{
    [SerializeField] private Image _imageRenderer;
    private string _idKindOfCustom;
    private string _idCustomDetail;
    private CustomSelection _customSelection;

    public void ConfigureButton(Sprite sprite, string idKindOfCustom,string idCustomDetail, CustomSelection customSelection)
    {
        _imageRenderer.sprite = sprite;
        _idKindOfCustom = idKindOfCustom;
        _customSelection = customSelection;
        _idCustomDetail = idCustomDetail;
    }

    public void OnClickSelectItem()
    {
        _customSelection.OnClickSelectItem(_idKindOfCustom,_idCustomDetail);
    }
}