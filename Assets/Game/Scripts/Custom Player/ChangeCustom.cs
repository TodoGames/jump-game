﻿using UnityEngine;

public class ChangeCustom : MonoBehaviour
{
    [SerializeField] public string _idKindOfCustom;
    [SerializeField] private Sprite _spriteToChange;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    private GameObject _prefabToChange;
    private GameObject _currentPrefab;

    public void SetSpriteToChange(Sprite spriteToChage)
    {
        _spriteToChange = spriteToChage;
    }

    public void SetPrefabToChange(GameObject prefabToChange)
    {
        _prefabToChange = prefabToChange;
    }

    public void OnClickChangeCustom()
    {
        //ChangeSprite();
        ChangeCurrentObject();
    }

    public void ChangeSprite()
    {
        if(_spriteToChange == null)
        {
            return;
        }
        _spriteRenderer.sprite = _spriteToChange;
    }

    public void ChangeCurrentObject()
    {
        if(_prefabToChange == null)
        {
            return;
        }
        if(_currentPrefab != null)
        {
            Destroy(_currentPrefab);
        }
        _currentPrefab = Instantiate(_prefabToChange, transform);
    }
}