using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangePanels : MonoBehaviour
{
    [SerializeField] private List<ButtonClickPanel> activeObjects = new List<ButtonClickPanel>();
    private void Start()
    {
        foreach (var item in activeObjects)
        {
            item.SetChangePanels(this);
        }
        DesactivePanels();
        activeObjects[0].ActivePanels();
    }
    public void DesactivePanels()
    {
        foreach (var item in activeObjects)
        {
            item.DesactivePanels();
        }
    }
}
