﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class GravityLimiter : MonoBehaviour
{
    [SerializeField] private float _minGravityLimiter;
    [SerializeField] private float _maxGravityLimiter;
    private Rigidbody2D _rigidbody2D;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        Vector2 currentRigidbody2DVelocity = _rigidbody2D.velocity;
        currentRigidbody2DVelocity.y = Mathf.Clamp(currentRigidbody2DVelocity.y,
            _minGravityLimiter, _maxGravityLimiter);
        _rigidbody2D.velocity = currentRigidbody2DVelocity;
    }

    public void Configuration(float minGravityLimiter, float maxGravityLimiter)
    {
        _minGravityLimiter = minGravityLimiter;
        _maxGravityLimiter = maxGravityLimiter;
    }
}