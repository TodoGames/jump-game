﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickPanel : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private List<GameObject> panelObjects;
    private ChangePanels _changePanels;

    private void Start()
    {
        button.onClick.AddListener(ActivePanels);
    }

    public void SetChangePanels(ChangePanels changePanels)
    {
        _changePanels = changePanels;
    }

    public void ActivePanels()
    {
        _changePanels.DesactivePanels();
        foreach (var item in panelObjects)
        {
            item.SetActive(true);
        }
    }

    public void DesactivePanels()
    {
        foreach (var item in panelObjects)
        {
            item.SetActive(false);
        }
    }
}
