using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignmentWithTheCamera : MonoBehaviour
{
    [SerializeField] private Vector3 _viewportPosition;
    void Start()
    {
        UpdatePositionToViewportToWorld();
    }

    void Update()
    {
#if UNITY_EDITOR
if(!Application.isPlaying)
{
        UpdatePositionToViewportToWorld();
}
#endif
    }

    [ContextMenu("UpdatePositionToViewportToWorld")]
    private void UpdatePositionToViewportToWorld()
    {
        Vector3 newPosition = Camera.main.ViewportToWorldPoint(_viewportPosition);
        newPosition.z = 0;
        transform.position = newPosition;
    }
}
