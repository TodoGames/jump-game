﻿using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using System;

public class PlayfabPlayerData
{
    public Action<GetUserDataResult> onGetUserDataSuccess;
    public Action onSetTileDataSuccess;
    public Action onError;
    
    public void SetPlayerTileData(Dictionary<string, string> data)
    {
        var request = new UpdateUserDataRequest()
        {
            Data = data
        };
        PlayFabClientAPI.UpdateUserData(request, OnSetTileDataSuccess, OnError);
    }
    private void OnSetTileDataSuccess(UpdateUserDataResult result)
    {
        onSetTileDataSuccess?.Invoke();
        Debug.Log("Actualizacion de datos completa");
    }
    public void GetPlayerData(string playFabId)
    {
        var request = new GetUserDataRequest()
        {
            PlayFabId = playFabId,            
        };
        PlayFabClientAPI.GetUserData(request, OnGetPlayerDataSuccess, OnError);
    }

    void OnGetPlayerDataSuccess(GetUserDataResult result)
    {
        onGetUserDataSuccess?.Invoke(result);
    }

    private void OnError(PlayFabError error)
    {
        onError?.Invoke();
        Debug.LogError("ERROR: " + error.ErrorMessage);
    }
}