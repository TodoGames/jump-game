using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.AdminModels;
using System;

public class InventoryLoader : MonoBehaviour
{
    [SerializeField] private string _catalogVersion = "Test";
    [SerializeField] private CustomBank _customBank;
    [SerializeField] private GameObject end_Object;

    private void Start()
    {
        CreateCatalogItems();
    }

    public void CreateCatalogItems()
    {
        //openStoreButton.interactable = false;
        var request = new GetCatalogItemsRequest()
        {
            CatalogVersion = _catalogVersion
        };
        PlayFabAdminAPI.GetCatalogItems(request, OnGetCatalogItemsSuccess, OnError);
    }

    private void OnGetCatalogItemsSuccess(GetCatalogItemsResult result)
    {
        Dictionary<string, KindOfCustoms> customs = new Dictionary<string, KindOfCustoms>();
        foreach (var item in result.Catalog)
        {
            if (!customs.ContainsKey(item.Tags[0]))
            {
                KindOfCustoms newKindOfCustom = new KindOfCustoms();
                newKindOfCustom._titleCustom = item.Tags[0];
                newKindOfCustom._customsDetails = new List<CustomDetails>();
                customs.Add(item.Tags[0], newKindOfCustom);
            }

            CustomDetails customDetails = new CustomDetails();
            customDetails._idCustom = item.ItemId;
            customDetails._nameCustom = item.DisplayName;
            CatalogItemCustomData catalogItemCustomData = JsonUtility.FromJson<CatalogItemCustomData>(item.CustomData);
            Debug.Log(catalogItemCustomData._urlPrefab);
            customDetails._prefabObject = Resources.Load<GameObject>(catalogItemCustomData._urlPrefab);
            customDetails._spriteImage = Resources.Load<Sprite>(item.ItemImageUrl);
            customs[item.Tags[0]]._customsDetails.Add(customDetails);
        }
        _customBank.SetNewCustoms(new List<KindOfCustoms>(customs.Values));
        end_Object.SetActive(true);
    }

    void OnError(PlayFabError error)
    {
        Debug.LogError(error.GenerateErrorReport());
    }
}

[Serializable]
public class CatalogItemCustomData
{
    public string _urlPrefab;
}