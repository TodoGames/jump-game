﻿public class HealthOfPlatformChangePlatform : AbstractHealth
{
    private PlatformPlayerController _platformPlayerController;

    public void SetPlatformPlayerController(PlatformPlayerController platformPlayerController)
    {
        _platformPlayerController = platformPlayerController;
    }
    public override void ActiveStateDeath()
    {
        base.ActiveStateDeath();
        if (_platformPlayerController)
        {
            _platformPlayerController.SetDefaultPlatform();
        }
    }
}