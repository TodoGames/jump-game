﻿public interface IDamageHealth
{
    public void DecrementHealth(int amount);
}