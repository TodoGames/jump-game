using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractHealth : MonoBehaviour, IHealHealth, IDamageHealth
{
    [SerializeField] protected int _currentHealth;
    [SerializeField] protected int _maxHealth;

    protected virtual void Start()
    {
        UpdateStateHealth();
    }

    public void SetMaxHealth(int maxHealth)
    {
        _maxHealth = maxHealth;
    }

    public void SetCurrentHealth(int currentHealth)
    {
        if(currentHealth >= _maxHealth)
        {
            currentHealth = _maxHealth;
        }
        _currentHealth = currentHealth;
    }

    public virtual void IncrementHealth(int amount)
    {
        _currentHealth += amount;
        if(_currentHealth > _maxHealth)
        {
            _currentHealth = _maxHealth;
        }
        UpdateStateHealth();
    }

    public virtual void DecrementHealth(int amount)
    {
        _currentHealth -= amount;
        UpdateStateHealth();
    }

    public virtual void UpdateStateHealth()
    {
        if (IsDeath())
        {
            ActiveStateDeath();
        }
    }

    public virtual void ActiveStateDeath()
    {
        _currentHealth = 0;
    }

    protected virtual bool IsDeath()
    {
        return _currentHealth <= 0;
    }

    public virtual int GetCurrentHealth()
    {
        return _currentHealth;
    }
}
