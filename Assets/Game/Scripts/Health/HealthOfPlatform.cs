﻿using UnityEngine;
using UnityEngine.UI;

public class HealthOfPlatform : AbstractHealth
{
    [SerializeField] private Text _healthText;
    [SerializeField] private GameObject _currentPlatformObject;

    public override void UpdateStateHealth()
    {
        base.UpdateStateHealth();
        if (_healthText != null)
        {
            _healthText.text = _currentHealth.ToString();
        }
    }

    public override void ActiveStateDeath()
    {
        base.ActiveStateDeath();
        Destroy(_currentPlatformObject);
    }
}
