﻿public interface IHealHealth
{
    public void IncrementHealth(int amount);
}
