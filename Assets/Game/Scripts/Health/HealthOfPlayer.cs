﻿using UnityEngine;

public class HealthOfPlayer : AbstractHealth
{
    public override void DecrementHealth(int amount)
    {
        base.DecrementHealth(amount);
        Handheld.Vibrate();
    }
    public override void UpdateStateHealth()
    {
        base.UpdateStateHealth();
        PlayerHUD._instance.SetHealthText(_currentHealth);
    }

    public override void ActiveStateDeath()
    {
        base.ActiveStateDeath();
        Destroy(gameObject);
    }
}