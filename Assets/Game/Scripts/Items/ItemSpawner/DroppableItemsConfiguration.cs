﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Droppable Items Configuration", menuName = "Configurations/Droppable Items Configuration")]
public class DroppableItemsConfiguration : ScriptableObject
{
    [SerializeField] private List<DroppableItem> _currentDroppableItems;
    [SerializeField] private int _factorProbability;
    [SerializeField] private int _dropChance;

    public List<DroppableItem> CurrentDroppablesItems { get { return _currentDroppableItems; } set { _currentDroppableItems = value; } }
    public int FactorProbability { get { return _factorProbability; } set { _factorProbability = value; } }
    public int DropChance { get { return _dropChance; } set { _dropChance = value; } }

    public void ChangeValueItem(string idItem, float newValue)
    {
        foreach (var item in _currentDroppableItems)
        {
            if(item.idItem == idItem)
            {
                item.probability = (int)newValue;
                break;
            }
        }
    }

    public int GetValue(string idItem)
    {
        foreach (var item in _currentDroppableItems)
        {
            if (item.idItem == idItem)
            {
                return item.probability;
            }
        }
        return 0; 
    }
}
