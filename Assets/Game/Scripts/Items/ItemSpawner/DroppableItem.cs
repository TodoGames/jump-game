﻿using System;

[Serializable]
public class DroppableItem
{
    public string idItem;
    public int probability;
    public AbstractItem _abstractItem;
}