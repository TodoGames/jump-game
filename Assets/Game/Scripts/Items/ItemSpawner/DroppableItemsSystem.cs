﻿using System.Collections.Generic;
using UnityEngine;

public class DroppableItemsSystem
{
    private List<DroppableItem> _droppableItems;
    private int _totalySpawnPercent;
    private int _factorProbability;
    private int _dropChance;

    public DroppableItemsSystem(DroppableItemsConfiguration droppableItemsConfiguration)
    {
        _droppableItems = new List<DroppableItem>(droppableItemsConfiguration.CurrentDroppablesItems);
        _totalySpawnPercent = 0;
        _factorProbability = droppableItemsConfiguration.FactorProbability;
        _dropChance = droppableItemsConfiguration.DropChance;
        foreach (var item in _droppableItems)
        {
            _totalySpawnPercent += item.probability;
        }
        MergeSortItems(_droppableItems);
    }

    public GameObject SpawnDroppableItem(Vector3 spawnPosition)
    {
        int newProbability = Random.Range(0, _totalySpawnPercent + 1);
        int currentProbability = newProbability * _factorProbability;
        int probabilityChance = Random.Range(0, 101);

        if (probabilityChance >= _dropChance)
        {
            return null;
        }

        if (currentProbability >= _totalySpawnPercent)
        {
            currentProbability = _totalySpawnPercent;
        }

        foreach (var droppableItem in _droppableItems)
        {
            if (currentProbability <= droppableItem.probability)
            {
                return GameObject.Instantiate(droppableItem._abstractItem, spawnPosition, Quaternion.identity).gameObject;
            }
            else
            {
                currentProbability -= droppableItem.probability;
            }
        }
        return null;
    }

    private void MergeSortItems(List<DroppableItem> droppableItems)
    {
        List<DroppableItem> right = new List<DroppableItem>();
        List<DroppableItem> left = new List<DroppableItem>();
        if (droppableItems.Count <= 1)
        {
            return;
        }
        else
        {
            int index = 0;
            int middle = droppableItems.Count / 2;//1 2 3 4 5 6 7
            for (int i = 0; i < middle; i++)
            {
                left.Add(droppableItems[i]);
                index++;
            }
            if (droppableItems.Count % 2 == 0)
            {
                for (int i = 0; i < middle; i++)
                {
                    right.Add(droppableItems[index]);
                    index++;
                }
            }
            else
            {
                for (int i = 0; i < middle + 1; i++)
                {
                    right.Add(droppableItems[index]);
                    index++;
                }
            }
            MergeSortItems(left);
            MergeSortItems(right);
            JoinDroppableItems(droppableItems, left, right);
        }
    }
    static public void JoinDroppableItems(List<DroppableItem> droppableItems, List<DroppableItem> left, List<DroppableItem> right)
    {
        int i = 0;
        int j = 0;
        for (int k = 0; k < droppableItems.Count; k++)
        {
            if (i >= left.Count)
            {
                droppableItems[k] = right[j];
                j++;
                continue;
            }
            if (j >= right.Count)
            {
                droppableItems[k] = left[i];
                i++;
                continue;
            }
            if (left[i].probability > right[j].probability)
            {
                droppableItems[k] = left[i];
                i++;
            }
            else
            {
                droppableItems[k] = right[j];
                j++;
            }
        }
    }
}