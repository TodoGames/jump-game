﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    [SerializeField] private List<ItemsSpawner> _itemsSpawners;
    [SerializeField] private string _tagPlayer;
    [SerializeField] private float _addDistance;
    [SerializeField] private Score _score;
    private List<int> _pointsToNext;
    private int _currentIndex;
    private void Start()
    {
        foreach (var item in _itemsSpawners)
        {
            item.InitializeItemSpawner();
        }
        Spawn();
        UpdatePosition();
        _pointsToNext = new List<int>();
        int easyPoints, mediumPoints, hardPoints;
        easyPoints = PlayerPrefs.GetInt("EASY_POINTS", 500);
        mediumPoints = PlayerPrefs.GetInt("MEDIUM_POINTS", 500);
        hardPoints = PlayerPrefs.GetInt("HARD_POINTS", 500);
        _pointsToNext.Add(easyPoints);
        _pointsToNext.Add(easyPoints + mediumPoints);
        _pointsToNext.Add(easyPoints + mediumPoints + hardPoints);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(_tagPlayer))
        {
            Spawn();
            UpdatePosition();
        }
    }

    public void Spawn()
    {
        foreach (var item in _itemsSpawners)
        {
            item.SpawnItems();
        }
    }

    public void UpdatePosition()
    {
        transform.position += Vector3.up * _addDistance;
    }

    public void ComparePointsToDifficulty()
    {
        if(_pointsToNext[_currentIndex] >= _score.GetCurrentPoints())
        {
            foreach (var item in _itemsSpawners)
            {
                item.IncrementDifficulty();
            }
        }
        _currentIndex++;
        if(_currentIndex >= _pointsToNext.Count)
        {
            _currentIndex = 0;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Vector3 newPosition = transform.position;
        newPosition.y = transform.position.y + _addDistance;
        Gizmos.DrawLine(newPosition + Vector3.right, newPosition + Vector3.left);
    }
}