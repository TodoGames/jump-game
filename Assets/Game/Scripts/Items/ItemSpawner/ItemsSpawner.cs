﻿using System.Collections.Generic;
using UnityEngine;

public class ItemsSpawner : MonoBehaviour
{
    [SerializeField] private List<DroppableItemsConfiguration> _droppableItemsConfiguration;
    [SerializeField] private List<Transform> _itemsPositionToSpawn;
    private DroppableItemsSystem _droppableItemsSystem;
    private int _currentDifficult;
    public void InitializeItemSpawner()
    {
        _currentDifficult = 0;
        _droppableItemsSystem = new DroppableItemsSystem(_droppableItemsConfiguration[_currentDifficult]);
    }

    public void SpawnItems()
    {
        foreach (var transformPosition in _itemsPositionToSpawn)
        {
            _droppableItemsSystem.SpawnDroppableItem(transformPosition.position);
        }
    }

    public void IncrementDifficulty()
    {
        _currentDifficult++;
        if(_currentDifficult >= _droppableItemsConfiguration.Count)
        {
            _currentDifficult = _droppableItemsConfiguration.Count - 1;
        }
        _droppableItemsSystem = new DroppableItemsSystem(_droppableItemsConfiguration[_currentDifficult]);
    }
}
