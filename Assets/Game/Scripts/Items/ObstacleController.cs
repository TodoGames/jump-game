using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    [SerializeField] private ObstacleConfigurationSO obstacleConfigurationSO;
    [SerializeField] private Transform scaleObject;
    [SerializeField] private MobilePlatform mobilePlatform;

    private void Awake()
    {
        scaleObject.localScale = obstacleConfigurationSO.scaleObstacle;
        mobilePlatform.SetColliderSizeForTouch(obstacleConfigurationSO.scaleTouch);
    }
}
