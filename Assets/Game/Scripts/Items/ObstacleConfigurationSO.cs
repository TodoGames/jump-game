﻿using UnityEngine;

[CreateAssetMenu(fileName = "Obstacle Configuration", menuName = "Configurations/ObstacleConfig")]
public class ObstacleConfigurationSO : ScriptableObject
{
    public Vector2 scaleObstacle;
    public Vector2 scaleTouch;
}
