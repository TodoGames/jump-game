﻿using UnityEngine;

public abstract class AbstractItem : MonoBehaviour
{
    [SerializeField] protected string _tagTarget;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(_tagTarget))
        {
            TakeItem(collision.gameObject);
        }
    }

    protected virtual void TakeItem(GameObject collisionGameObject)
    {
        //Destroy(gameObject);
        gameObject.SetActive(false);
    }
}