using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealHealthItem : AbstractItem
{
    [SerializeField] private int _amountHeal;
    protected override void TakeItem(GameObject collisionGameObject)
    {
        var healHealth = collisionGameObject.GetComponent<IHealHealth>();
        if(healHealth != null)
        {
            healHealth.IncrementHealth(_amountHeal);
        }
        base.TakeItem(collisionGameObject);
    }
}
