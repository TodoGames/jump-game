﻿using UnityEngine;

public class IceItem : AbstractItem
{
    protected override void TakeItem(GameObject collisionGameObject)
    {
        var rigidbody2D = collisionGameObject.GetComponent<Rigidbody2D>();
        if (rigidbody2D != null)
        {
            rigidbody2D.velocity = Vector2.zero;
        }
        base.TakeItem(collisionGameObject);
    }
}