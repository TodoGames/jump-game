﻿using UnityEngine;

public class SkillItem : AbstractItem
{
    [SerializeField] private int _amountPowerSkill;
    protected override void TakeItem(GameObject collisionGameObject)
    {
        if(PlatformButtonsGenerator.Instance != null)
        {
            PlatformButtonsGenerator.Instance.IncrementPowerSkill(_amountPowerSkill);
        }
        base.TakeItem(collisionGameObject);
    }
}
