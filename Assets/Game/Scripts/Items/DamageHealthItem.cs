﻿using UnityEngine;

public class DamageHealthItem : AbstractItem
{
    [SerializeField] private int _amountDamage;
    protected override void TakeItem(GameObject collisionGameObject)
    {
        var damageHealth = collisionGameObject.GetComponent<IDamageHealth>();
        if (damageHealth != null)
        {
            damageHealth.DecrementHealth(_amountDamage);
        }
    }
}