﻿using UnityEngine;

public class GameOverMenu : MonoBehaviour
{
    public static GameOverMenu _instance;
    [SerializeField] private GameObject _panelGameOver;
    [SerializeField] private GameObject _player;

    private void Awake()
    {
        _instance = this;
    }

    private void Update()
    {
        if(_player == null && !_panelGameOver.activeSelf)
        {
            SetGameOver();
        }
    }

    public void SetGameOver()
    {
        _panelGameOver.SetActive(true);
    }
}