using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour
{
    public static PlayerHUD _instance;

    [SerializeField] private Text _pointText;
    [SerializeField] private Text _healthText;
    private void Awake()
    {
        _instance = this;
    }
    public void SetPointText(string points)
    {
        _pointText.text = "Puntos: " + points;
    }

    public void SetHealthText(int healthPoints)
    {
        _healthText.text = "Vidas: " + healthPoints;
    }
}
