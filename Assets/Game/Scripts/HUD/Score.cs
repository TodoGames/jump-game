using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private CameraController _cameraController;
    [SerializeField] public float _maxScoreToDifficult;
    private float _startYposition;
    private float _currentPoints;
    private void Start()
    {
        _startYposition = _cameraController.GetCurrentYPosition();
        _maxScoreToDifficult = PlayerPrefs.GetFloat("MaxSCoreToDifficult", 500f);
    }

    private void Update()
    {
        _currentPoints = Mathf.Abs(_cameraController.GetCurrentYPosition() - _startYposition);
        PlayerHUD._instance.SetPointText(Mathf.RoundToInt(_currentPoints).ToString());
    }

    public float GetCurrentScoreProgress()
    {
        return Mathf.Clamp01(GetCurrentPoints() / _maxScoreToDifficult);
    }

    public float GetCurrentPoints()
    {
        return _currentPoints;
    }
}
