using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatePlatformOnClick : MonoBehaviour
{
    [SerializeField] private Image _imageToFill;
    [SerializeField] private Text _platformNameText;
    [SerializeField] private PlatformController _platformControllerToSpawn;
    private InformationOfPlatformToCreate _informationOfPlatformToCreate;
    private float _currentTimeToActive;
    private int _currentItemsTaken;
    public bool _isSpawnMobilePlatformAvailable { get; private set; }
    private bool _inProgressSpawn;

    private void Update()
    {
        if (_inProgressSpawn)
        {
            return;
        }
        CoolDownToClick();
    }

    private void CoolDownToClick()
    {
        if (_informationOfPlatformToCreate.IsCoolDownAutomatic && !_isSpawnMobilePlatformAvailable)
        {
            if (_currentTimeToActive < _informationOfPlatformToCreate.TimeOfCoolDown)
            {
                _currentTimeToActive += Time.deltaTime;
            }
            else
            {
                _currentTimeToActive = _informationOfPlatformToCreate.TimeOfCoolDown;
                _isSpawnMobilePlatformAvailable = true;
            }
            UpdateFillImage(_currentTimeToActive, _informationOfPlatformToCreate.TimeOfCoolDown);
        }
    }

    public void SetConfiguration(InformationOfPlatformToCreate informationOfPlatformToCreate)
    {
        _platformControllerToSpawn = informationOfPlatformToCreate.PlatformControllerPrefab;
        _informationOfPlatformToCreate = informationOfPlatformToCreate;
        _platformNameText.text = _informationOfPlatformToCreate.NamePlatform;
        _currentItemsTaken = 0;
        if (!_informationOfPlatformToCreate.IsCoolDownAutomatic)
        {
            _isSpawnMobilePlatformAvailable = false;
            IncrementItemsTaken(_currentItemsTaken);
        }
        else
        {
            _isSpawnMobilePlatformAvailable = true;
            _currentTimeToActive = _informationOfPlatformToCreate.TimeOfCoolDown;
            UpdateFillImage(_currentTimeToActive, _informationOfPlatformToCreate.TimeOfCoolDown);
        }
    }

    public PlatformController SpawnPlatform(Vector3 positionToSpawn)
    {
        return SpawnPlatform(0, positionToSpawn);
    }

    public PlatformController SpawnPlatform(int indexFinger, Vector3 positionToSpawn)
    {
        if (!_isSpawnMobilePlatformAvailable)
        {
            return null;
        }

        if (!PlatformButtonsGenerator.Instance.IsActivePanelGenerator())
        {
            return null;
        }

        _isSpawnMobilePlatformAvailable = false;
        _inProgressSpawn = true;

        Quaternion currentRotation = _platformControllerToSpawn.transform.rotation;
        PlatformController _currentPlatformControllerSpawned = Instantiate(_platformControllerToSpawn, positionToSpawn, currentRotation);
        _currentPlatformControllerSpawned.SetInitalSpawnConfiguration(this, indexFinger);
        _imageToFill.color = Color.cyan;
        return _currentPlatformControllerSpawned;
    }

    public void CancelSpawn()
    {
        _imageToFill.fillAmount = 1;
        _currentTimeToActive = _informationOfPlatformToCreate.TimeOfCoolDown;
        _currentItemsTaken = _informationOfPlatformToCreate.MaxItemsToActive;
        _isSpawnMobilePlatformAvailable = true;

        ResetCurrentSpawnPlatform();
    }

    public void EndSpawnPlatform()
    {
        _imageToFill.fillAmount = 0;
        _currentTimeToActive = 0;
        _currentItemsTaken = 0;
        _isSpawnMobilePlatformAvailable = false;

        ResetCurrentSpawnPlatform();
    }

    private void ResetCurrentSpawnPlatform()
    {
        _inProgressSpawn = false;
        _imageToFill.color = Color.white;
    }

    public void IncrementItemsTaken(int amount)
    {
        _currentItemsTaken += amount;
        if(_currentItemsTaken >= _informationOfPlatformToCreate.MaxItemsToActive)
        {
            _isSpawnMobilePlatformAvailable = true;
        }
        UpdateFillImage((float)_currentItemsTaken, (float)_informationOfPlatformToCreate.MaxItemsToActive);
    }

    public void UpdateFillImage(float currentValue, float maxValue)
    {
        if(maxValue <= 0)
        {
            _imageToFill.fillAmount = 1;
        }
        else
        {
            _imageToFill.fillAmount = currentValue / maxValue;
        }
    }

    public InformationOfPlatformToCreate GetInformationOfPlatformToCreate()
    {
        return _informationOfPlatformToCreate;
    }
}
