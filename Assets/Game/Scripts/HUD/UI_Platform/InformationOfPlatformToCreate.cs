﻿using System;
using UnityEngine;

[Serializable]
public class InformationOfPlatformToCreate
{
    [SerializeField] private string _idPlatform;
    [SerializeField] private string _namePlatform;
    [SerializeField] private PlatformController _platformControllerPrefab;
    [SerializeField] private float _timeOfCoolDown;
    [SerializeField] private bool _isCoolDownAutomatic;
    [SerializeField] private int _maxItemsToActive;

    public string ID => _idPlatform;
    public string NamePlatform => _namePlatform;
    public PlatformController PlatformControllerPrefab => _platformControllerPrefab;
    public float TimeOfCoolDown => _timeOfCoolDown;
    public bool IsCoolDownAutomatic => _isCoolDownAutomatic;
    public int MaxItemsToActive => _maxItemsToActive;

    public void SetCoolDown(float amountCooldown)
    {
        _timeOfCoolDown = amountCooldown;
    }

    public void SetMaxItemsToActive(int quantity)
    {
        _maxItemsToActive = quantity;
    }
}