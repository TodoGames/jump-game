﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlatformButtonsGenerator : MonoBehaviour
{
    public static PlatformButtonsGenerator Instance { get; private set; }
    [SerializeField] private ConfigurationOfPlatformToCreate _configurationOfPlatformToCreate;
    [Header("Settings Panel")]
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private GameObject _imageCoolDown;
    [Header("Create Platform On Click")]
    [SerializeField] private CreatePlatformOnClick _createPlatformOnClickPrefab;
    [SerializeField] private Transform _parentToCreate;
    private List<CreatePlatformOnClick> _currentSkills = new List<CreatePlatformOnClick>();

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        foreach (var informationOfPlatformToCreate in _configurationOfPlatformToCreate.GetConfigurationOfPlatforms())
        {
            CreatePlatformOnClick(informationOfPlatformToCreate);
        }
    }
    public void CreatePlatformOnClick(InformationOfPlatformToCreate informationOfPlatformToCreate)
    {
        CreatePlatformOnClick newCreatePlatformOnClick = Instantiate(_createPlatformOnClickPrefab, _parentToCreate);
        newCreatePlatformOnClick.SetConfiguration(informationOfPlatformToCreate);
        if (!informationOfPlatformToCreate.IsCoolDownAutomatic)
        {
            _currentSkills.Add(newCreatePlatformOnClick);
        }
    }

    public void DesactivePanelGenerator()
    {
        _canvasGroup.interactable = false;
        _imageCoolDown.SetActive(true);
    }

    public void ActivePanelGenerator()
    {
        _canvasGroup.interactable = true;
        _imageCoolDown.SetActive(false);
    }

    public bool IsActivePanelGenerator()
    {
        return _canvasGroup.interactable;
    }

    public void IncrementPowerSkill(int amount)
    {
        foreach (var skills in _currentSkills)
        {
            skills.IncrementItemsTaken(amount);
        }
    }
}
