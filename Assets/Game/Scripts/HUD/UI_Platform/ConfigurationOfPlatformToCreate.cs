﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Platforms Configuration",menuName = "Configurations/Platforms Configuration")]
public class ConfigurationOfPlatformToCreate: ScriptableObject
{
    [SerializeField] private List<InformationOfPlatformToCreate> informationOfPlatformsToCreates;

    public List<InformationOfPlatformToCreate> GetConfigurationOfPlatforms()
    {
        return informationOfPlatformsToCreates;
    }

    public void SetCoolDown(string idPlatform, float value)
    {
        foreach (var item in informationOfPlatformsToCreates)
        {
            if(item.ID == idPlatform)
            {
                item.SetCoolDown(value);
            }
        }
    }

    public void SetMaxItemsToActive(string idPlatform, int value)
    {
        foreach (var item in informationOfPlatformsToCreates)
        {
            if (item.ID == idPlatform)
            {
                item.SetMaxItemsToActive(value);
            }
        }
    }

    public float GetCooldown(string idPlatform)
    {
        foreach (var item in informationOfPlatformsToCreates)
        {
            if (item.ID == idPlatform)
            {
                return item.TimeOfCoolDown;
            }
        }
        return 0;
    }

    public int GetMaxItemsToActive(string idPlatform)
    {
        foreach (var item in informationOfPlatformsToCreates)
        {
            if (item.ID == idPlatform)
            {
                return item.MaxItemsToActive;
            }
        }
        return 0;
    }
}
