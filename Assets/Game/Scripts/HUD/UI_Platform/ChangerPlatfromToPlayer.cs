﻿using UnityEngine;
using UnityEngine.UI;

public class ChangerPlatfromToPlayer : MonoBehaviour
{
    [SerializeField] private CreatePlatformOnClick _createPlatformOnClick;
    [SerializeField] private Button _changeButton;
    private PlatformPlayerController _platformPlayerController;

    private void Start()
    {
        _platformPlayerController = FindObjectOfType<PlatformPlayerController>();
        _changeButton.onClick.AddListener(OnClickChange);
    }

    private void Update()
    {
        _changeButton.interactable = _createPlatformOnClick._isSpawnMobilePlatformAvailable;
    }

    public void OnClickChange()
    {
        if(_platformPlayerController != null)
        {
            PlatformController _newPlatformcontroller = _createPlatformOnClick.SpawnPlatform(_platformPlayerController.transform.position);
            if(_newPlatformcontroller != null)
            {
                _platformPlayerController.SetCurrentPlatformController(_newPlatformcontroller);
                _createPlatformOnClick.EndSpawnPlatform();
                if (!_createPlatformOnClick.GetInformationOfPlatformToCreate().IsCoolDownAutomatic)
                {
                    HealthOfPlatformChangePlatform currentHealth = _newPlatformcontroller.GetComponentInChildren<HealthOfPlatformChangePlatform>();
                    if (currentHealth != null)
                    {
                        currentHealth.SetPlatformPlayerController(_platformPlayerController);
                    }
                }
            }
        }
    }
}