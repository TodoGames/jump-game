using System;
using UnityEngine;

public class GlobalInstaller : MonoBehaviour
{
    private void Start()
    {
        InitGame();
    }

    void InitGame()
    {
        var playFabGoogleServicesLogin = new PlayFabGPGSLogin();
        var loginUseCase = new LoginUseCase(playFabGoogleServicesLogin);
        var initializeGameUseCase = new InitializeGameUseCase(loginUseCase);
        
        initializeGameUseCase.InitGame();
    }
}