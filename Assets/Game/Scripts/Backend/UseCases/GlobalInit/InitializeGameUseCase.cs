using UnityEngine;
using UnityEngine.SceneManagement;

public class InitializeGameUseCase
{
    private readonly ILoginRequester _loginRequester;
    
    public InitializeGameUseCase(ILoginRequester loginRequester)
    {
        _loginRequester = loginRequester;
    }
    
    public async void InitGame()
    {
        await _loginRequester.Login();
        Debug.Log("logged in");
        SceneManager.LoadSceneAsync("Test_DrawnSpawn");
    }
}