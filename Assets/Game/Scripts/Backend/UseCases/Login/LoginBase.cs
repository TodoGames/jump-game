using Cysharp.Threading.Tasks;
using UnityEngine;

public class LoginBase : ILoginService
{
    public UniTaskCompletionSource<bool> UTask = new UniTaskCompletionSource<bool>();
    public virtual UniTask Login()
    {
        UTask = new UniTaskCompletionSource<bool>();
        return UTask.Task;
    }

    public virtual void Init()
    {
        
    }
}