using Cysharp.Threading.Tasks;
using UnityEngine;

public class LoginUseCase : ILoginRequester
{
    private readonly ILoginService _loginService;

    public LoginUseCase(ILoginService authenticateService)
    {
        _loginService = authenticateService;
    }
    public async UniTask Login()
    {
        await _loginService.Login();
    }
}