using System.Threading.Tasks;
using Cysharp.Threading.Tasks;

public interface ILoginService
{
    void Init();
    UniTask Login();
}