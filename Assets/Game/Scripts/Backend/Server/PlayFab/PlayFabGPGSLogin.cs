using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Cysharp.Threading.Tasks;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

public class PlayFabGPGSLogin : LoginBase
{
    private PlayGamesClientConfiguration _clientConfiguration;
    
    public override void Init()
    {
        _clientConfiguration = new PlayGamesClientConfiguration.Builder().AddOauthScope("profile").RequestServerAuthCode(false).Build();
    }

    public override UniTask Login()
    {
        UTask = new UniTaskCompletionSource<bool>();
        
        PlayGamesPlatform.InitializeInstance(_clientConfiguration);
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce, OnSignIn);
        
        return UTask.Task;
    }
    
    private void OnSignIn(SignInStatus status) {
        if (status == SignInStatus.Success)
        {
            var playfabAuthCode = PlayGamesPlatform.Instance.GetServerAuthCode();
            Debug.Log("first auth code: " + playfabAuthCode);
            PlayGamesPlatform.Instance.GetAnotherServerAuthCode(false, Callback);
        }
        else
        {
            Debug.LogFormat("LOGIN ERROR STATUS: " + status);
        }
    }

    private void Callback(string obj)
    {
        PlayFabLogin(obj, UTask);
    }

    public void PlayFabLogin(string oAuthCode, UniTaskCompletionSource<bool> t)
    {
        var loginWithGPGSRequest = new LoginWithGooglePlayGamesServicesRequest()
        {
            TitleId = PlayFabSettings.TitleId,
            CreateAccount = true,
            ServerAuthCode = oAuthCode
        };
        PlayFabClientAPI.LoginWithGooglePlayGamesServices(loginWithGPGSRequest, (result) => OnSuccess(result, t),
            error => OnError(error, t));
    }

    private void OnSuccess(LoginResult result, UniTaskCompletionSource<bool> taskCompletionSource)
    {
#if !UNITY_EDITOR
        var updateUserTitleDisplayNameRequest = new UpdateUserTitleDisplayNameRequest()
        {
            DisplayName = PlayGamesPlatform.Instance.GetUserDisplayName()
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(updateUserTitleDisplayNameRequest, nameResult =>
        {
            Debug.Log("updated display name for android device to: " + nameResult.DisplayName);
        }, error =>
        {
            Debug.Log(error.GenerateErrorReport());
        });
#endif
        
        taskCompletionSource.TrySetResult(true);
    }
    
    private void OnError(PlayFabError error, UniTaskCompletionSource<bool> taskCompletionSource)
    {
        taskCompletionSource.TrySetResult(false);
        var message = "";
        if (error.ErrorDetails != null)
        {
            foreach (var errorDetail in error.ErrorDetails)
            {
                foreach (var detail in errorDetail.Value)
                {
                    message += detail + "\n";
                }
            }
        }
        Debug.Log(error.GenerateErrorReport());
    }
}
