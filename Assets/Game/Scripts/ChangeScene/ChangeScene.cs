﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    [SerializeField] private string _nameNextScene;
    public void OnChangeScene()
    {
        SceneManager.LoadScene(_nameNextScene);
    }
}
