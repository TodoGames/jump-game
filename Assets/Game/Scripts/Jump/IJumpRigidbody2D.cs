﻿using UnityEngine;

public interface IJumpRigidbody2D
{
    public void DoJump(Rigidbody2D _rigidbody2D);
}
