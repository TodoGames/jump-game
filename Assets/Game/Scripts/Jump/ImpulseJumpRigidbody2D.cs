﻿using UnityEngine;

public class ImpulseJumpRigidbody2D : MonoBehaviour, IJumpRigidbody2D
{
    public enum HorizontalDirection
    {
        Right,
        Left,
        Random,
        Opposite,
        Center,
        Normal
    }

    [SerializeField] private float _forceToJump;
    [SerializeField] private float _forceToHorizontalMovement;
    [SerializeField] private HorizontalDirection _horizontalDirectionOfJump;

    public void Configuration(float forceToJump, float forceHorizontalMovement)
    {
        _forceToJump = forceToJump;
        _forceToHorizontalMovement = forceHorizontalMovement;
    }

    public virtual void DoJump(Rigidbody2D _rigidbody2D)
    {
        Vector2 directionToJump = Vector2.up;
        switch (_horizontalDirectionOfJump)
        {
            case HorizontalDirection.Right:
                directionToJump.x = 1f * _forceToHorizontalMovement;
                break;
            case HorizontalDirection.Left:
                directionToJump.x = -1f * _forceToHorizontalMovement;
                break;
            case HorizontalDirection.Random:
                directionToJump.x = Random.Range(-0.9f, 0.9f) * _forceToHorizontalMovement;
                break;
            case HorizontalDirection.Opposite:
                directionToJump.x = (_rigidbody2D.velocity.x * -1) * _forceToHorizontalMovement;
                break;
            case HorizontalDirection.Center:
                directionToJump.x = 0f;
                break;
            case HorizontalDirection.Normal:
                directionToJump.x = _rigidbody2D.velocity.x;
                break;
            default:
                break;
        }

        _rigidbody2D.velocity = Vector2.zero;
        directionToJump.y *= _forceToJump;
        _rigidbody2D.AddForce(directionToJump, ForceMode2D.Impulse);

        if (_horizontalDirectionOfJump == HorizontalDirection.Normal)
        {
            Vector2 currentVelocity = _rigidbody2D.velocity;
            currentVelocity.x = directionToJump.x;
            _rigidbody2D.velocity = currentVelocity;
        }
    }
}