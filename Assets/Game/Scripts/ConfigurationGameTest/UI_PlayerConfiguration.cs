﻿using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerConfiguration : MonoBehaviour
{
    [SerializeField] private PlayerConfigurationSO playerConfigurationSO;
    [SerializeField] private Text information;

    [Header("Sliders")]
    [SerializeField] private Slider _sliderJump;
    [SerializeField] private Slider _sliderFall;
    [SerializeField] private Slider _sliderHealth;

    private void Start()
    {
        SliderConfigurations();
        SetInitialValues();
    }

    private void SliderConfigurations()
    {
        _sliderJump.onValueChanged.AddListener(SetJump);
        _sliderFall.onValueChanged.AddListener(SetFall);
        _sliderHealth.onValueChanged.AddListener(SetHealth);
    }

    public void SetInitialValues()
    {
        _sliderFall.value = playerConfigurationSO.maxFall;
        _sliderHealth.value = playerConfigurationSO.initialHealth;
        _sliderJump.value = playerConfigurationSO.maxJump;
    }

    private void Update()
    {
        information.text = string.Format("VEL SALTO: {0}\nVEL CAIDA: {1}\nVIDAS INICIAL: {2}",
            _sliderJump.value,_sliderFall.value,_sliderHealth.value);
    }

    public void SetJump(float value)
    {
        playerConfigurationSO.maxJump = value;
    }

    public void SetFall(float value)
    {
        playerConfigurationSO.maxFall = value;
    }

    public void SetHealth(float value)
    {
        playerConfigurationSO.initialHealth = (int)value;
    }
}