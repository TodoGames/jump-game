﻿using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerPrefsConfiguration : MonoBehaviour
{
    [SerializeField] private string _nameOfPlayerPref;
    [SerializeField] private Text _textInformation;
    [SerializeField] private Slider _sliderForPlayerPrefs;

    private void Start()
    {
        _sliderForPlayerPrefs.value = PlayerPrefs.GetFloat(_nameOfPlayerPref, 1f);
        _sliderForPlayerPrefs.onValueChanged.AddListener(SetPlayerPrefsConfiguration);
    }

    private void Update()
    {
        _textInformation.text = _sliderForPlayerPrefs.value.ToString();
    }

    public void SetPlayerPrefsConfiguration(float value)
    {
        PlayerPrefs.SetFloat(_nameOfPlayerPref, value);
    }
}