using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PlatformConfiguration : MonoBehaviour
{
    [SerializeField] private PlatformConfigurationSO _platformConfigurationSO;
    [SerializeField] private ConfigurationOfPlatformToCreate _configurationOfPlatformToCreate;
    [SerializeField] private Transform scalePlatform;
    [SerializeField] private Transform scaleTouch;
    [SerializeField] private Text _textInformation;
    [SerializeField] private string _idPlatform;
    [Header("Sliders")]
    [SerializeField] private Slider _sliderXPlatform;
    [SerializeField] private Slider _sliderYPlatform;
    [SerializeField] private Slider _sliderXTouch;
    [SerializeField] private Slider _sliderYTouch;
    [SerializeField] private Slider _sliderJump;
    [SerializeField] private Slider _sliderHorizontal;
    [SerializeField] private Slider _sliderMaxHealth;
    [SerializeField] private Slider _sliderCooldown;
    [SerializeField] private Slider _sliderMaxItems;
    void Start()
    {
        SliderConfigurations();
        SetInitialValues();
    }

    private void SliderConfigurations()
    {
        _sliderXPlatform.onValueChanged.AddListener(SetScaleXPlatfrom);
        _sliderYPlatform.onValueChanged.AddListener(SetScaleYPlatfrom);
        _sliderXTouch.onValueChanged.AddListener(SetScaleXTouch);
        _sliderYTouch.onValueChanged.AddListener(SetScaleYTouch);
        _sliderJump.onValueChanged.AddListener(SetJump);
        _sliderHorizontal.onValueChanged.AddListener(SetHorizontal);
        _sliderMaxHealth.onValueChanged.AddListener(SetHealth);
        if (_sliderCooldown != null)
        {
            _sliderCooldown.onValueChanged.AddListener(SetCoolDown);
        }

        if (_sliderMaxItems != null)
        {
            _sliderMaxItems.onValueChanged.AddListener(SetMaxItems);
        }
    }

    public void SetInitialValues()
    {
        _sliderXPlatform.value = _platformConfigurationSO._platformScale.x;
        _sliderYPlatform.value = _platformConfigurationSO._platformScale.y;
        _sliderXTouch.value = _platformConfigurationSO._touchScale.x;
        _sliderYTouch.value = _platformConfigurationSO._touchScale.y;
        _sliderJump.value = _platformConfigurationSO._forceToJump;
        _sliderHorizontal.value = _platformConfigurationSO._forceHorizontal;
        _sliderMaxHealth.value = _platformConfigurationSO._maxHealth;
        if (_sliderCooldown != null)
        {
            _sliderCooldown.value = _configurationOfPlatformToCreate.GetCooldown(_idPlatform);
        }
        if (_sliderMaxItems != null)
        {
            _sliderMaxItems.value = _configurationOfPlatformToCreate.GetMaxItemsToActive(_idPlatform);
        }
    }

    void Update()
    {
        scalePlatform.localScale = _platformConfigurationSO._platformScale;
        scaleTouch.localScale = _platformConfigurationSO._touchScale;
        _textInformation.text = String.Format("ESCALA:{0}\nTOUCH: {1}\nSALTO: {2}\nMOVIMIENTO HORIZONTAL: {3}\nMAX VIDAS: {4}\nCOOLDOWN: {5}\nMAX ITEMS: {6}",
            _platformConfigurationSO._platformScale,_platformConfigurationSO._touchScale,_platformConfigurationSO._forceToJump,_platformConfigurationSO._forceHorizontal,
            _platformConfigurationSO._maxHealth, _configurationOfPlatformToCreate.GetCooldown(_idPlatform), _configurationOfPlatformToCreate.GetMaxItemsToActive(_idPlatform));
    }

    public void SetScaleXPlatfrom(float value)
    {
        _platformConfigurationSO._platformScale.x = value;

    }

    public void SetScaleYPlatfrom(float value)
    {
        _platformConfigurationSO._platformScale.y = value;
    }
    public void SetScaleXTouch(float value)
    {
        _platformConfigurationSO._touchScale.x = value;
    }

    public void SetScaleYTouch(float value)
    {
        _platformConfigurationSO._touchScale.y = value;
    }

    public void SetJump(float value)
    {
        _platformConfigurationSO._forceToJump = value;
    }

    public void SetHorizontal(float value)
    {
        _platformConfigurationSO._forceHorizontal = value;
    }

    public void SetHealth(float value)
    {
        _platformConfigurationSO._maxHealth = (int)value;
    }

    public void SetCoolDown(float value)
    {
        //_platformConfigurationSO._coolDown = value;
        _configurationOfPlatformToCreate.SetCoolDown(_idPlatform, value);
    }

    public void SetMaxItems(float value)
    {
        _configurationOfPlatformToCreate.SetMaxItemsToActive(_idPlatform, (int)value);
    }
}
