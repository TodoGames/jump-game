﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

public class SaveLoadGameTest : MonoBehaviour
{
    public const string _keyDefaultSettings = "DefaultSettings";
    [SerializeField] private Button _saveButton;
    [SerializeField] private Button _loadButton;
    [SerializeField] private Button _resetButton;
    [SerializeField] private GameObject _panelSaveLoad;
    [SerializeField] private InputField _nameSavedInputField;
    [SerializeField] private Text _pathText;
    [SerializeField] private Text _informationText;
    [SerializeField] private string _nameResetTest;
    [Header("Configurations")]
    [SerializeField] private ConfigurationsController _configurationsController;
    [SerializeField] private List<UI_DifficultyConfiguration> uI_DifficultyConfigurations = new List<UI_DifficultyConfiguration>();
    [SerializeField] private List<UI_ObstacleConfiguration> uI_ObstacleConfigurations = new List<UI_ObstacleConfiguration>();
    [SerializeField] private List<UI_PlatformConfiguration> uI_PlatformConfigurations = new List<UI_PlatformConfiguration>();
    [SerializeField] private List<UI_PlayerConfiguration> uI_PlayerConfigurations = new List<UI_PlayerConfiguration>();
    [SerializeField] private List<UI_DifficultyProgressConfiguration> uI_DifficultyProgressConfiguration = new List<UI_DifficultyProgressConfiguration>();
    private void Start()
    {
        _saveButton.onClick.AddListener(SaveTest);
        _loadButton.onClick.AddListener(LoadTestOnInputField);
        _resetButton.onClick.AddListener(ResetTest);
        _pathText.text = "RUTA: " + Application.persistentDataPath;

        if (string.IsNullOrEmpty(PlayerPrefs.GetString(_keyDefaultSettings, "")))
        {
            PlayerPrefs.SetString(_keyDefaultSettings, _configurationsController.GetConfigurationOnJson());
            Debug.Log("<Color=green>Default values saved</Color>");
        }
    }

    public void SaveTest()
    {
        if (string.IsNullOrEmpty(_nameSavedInputField.text))
        {
            SetTextInformation(Color.red, "Debes poner un nombre al archivo");
            return;
        }

        string nameFile = "/"+_nameSavedInputField.text + ".txt";
        string path = Application.persistentDataPath + nameFile;
        var writing = new StreamWriter(path);
        if (!File.Exists(path))
        {
            writing =  File.CreateText(path);
        }
        writing.Write(_configurationsController.GetConfigurationOnJson());
        writing.Close();
        Debug.Log("Datos Guardados: " + _configurationsController.GetConfigurationOnJson());
        SetTextInformation(Color.green, "Exito al guardar");
    }

    public void LoadTest(string nameFileToLoad)
    {
        if (string.IsNullOrEmpty(nameFileToLoad))
        {
            SetTextInformation(Color.red, "Debes poner un nombre al archivo");
            return;
        }

        string nameFile = "/" + nameFileToLoad + ".txt";
        string path = Application.persistentDataPath;
        if (!File.Exists(Application.persistentDataPath + nameFile))
        {
            SetTextInformation(Color.red, "No se encontro el archivo");
            return;
        }
        path = Application.persistentDataPath + nameFile;
        var reading = new StreamReader(path);
        _configurationsController.SetConfiguration(reading.ReadToEnd());
        reading.Close();
        ReInitializerConfigurations();
        SetTextInformation(Color.green, "Exito al cargar");
    }

    public void ResetTest()
    {
        string defaultValues = PlayerPrefs.GetString(_keyDefaultSettings, "");
        if (!string.IsNullOrEmpty(defaultValues))
        {
            _configurationsController.SetConfiguration(defaultValues);
        }
        ReInitializerConfigurations();
    }

    public void LoadTestOnInputField()
    {
        LoadTest(_nameSavedInputField.text);
    }

    public void SetTextInformation(Color color, string information)
    {
        _informationText.color = color;
        _informationText.text = information;
        Invoke("ResetInformation", 1.5f);
    }

    public void ResetInformation()
    {
        _informationText.text = "";
    }

    public void ReInitializerConfigurations()
    {
        foreach (var item in uI_DifficultyConfigurations)
        {
            item.SetInitialValues();
        }

        foreach (var item in uI_ObstacleConfigurations)
        {
            item.SetInitialValues();
        }

        foreach (var item in uI_PlatformConfigurations)
        {
            item.SetInitialValues();
        }

        foreach (var item in uI_PlayerConfigurations)
        {
            item.SetInitialValues();
        }

        foreach (var item in uI_DifficultyProgressConfiguration)
        {
            item.SetInitialValues();
        }
    }
}
