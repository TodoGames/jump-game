﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UI_ObstacleConfiguration : MonoBehaviour
{
    [SerializeField] private ObstacleConfigurationSO _obstacleConfiguration;
    [SerializeField] private Transform scalePlatform;
    [SerializeField] private Transform scaleTouch;
    [SerializeField] private Text _textInformation;
    [Header("Sliders")]
    [SerializeField] private Slider _sliderXPlatform;
    [SerializeField] private Slider _sliderYPlatform;
    [SerializeField] private Slider _sliderXTouch;
    [SerializeField] private Slider _sliderYTouch;
    void Start()
    {
        SliderConfigurations();
        SetInitialValues();
    }

    private void SliderConfigurations()
    {
        _sliderXPlatform.onValueChanged.AddListener(SetScaleXPlatfrom);
        _sliderYPlatform.onValueChanged.AddListener(SetScaleYPlatfrom);
        _sliderXTouch.onValueChanged.AddListener(SetScaleXTouch);
        _sliderYTouch.onValueChanged.AddListener(SetScaleYTouch);
    }

    public void SetInitialValues()
    {
        _sliderYPlatform.value = _obstacleConfiguration.scaleObstacle.y;
        _sliderXTouch.value = _obstacleConfiguration.scaleTouch.x;
        _sliderYTouch.value = _obstacleConfiguration.scaleTouch.y;
        _sliderXPlatform.value = _obstacleConfiguration.scaleObstacle.x;
    }
    void Update()
    {
        scalePlatform.localScale = _obstacleConfiguration.scaleObstacle;
        scaleTouch.localScale = _obstacleConfiguration.scaleTouch;
        _textInformation.text = String.Format("ESCALA:{0}\nTOUCH: {1}",
            _obstacleConfiguration.scaleObstacle,_obstacleConfiguration.scaleTouch);
    }


    public void SetScaleXPlatfrom(float value)
    {
        _obstacleConfiguration.scaleObstacle.x = value;

    }

    public void SetScaleYPlatfrom(float value)
    {
        _obstacleConfiguration.scaleObstacle.y = value;
    }
    public void SetScaleXTouch(float value)
    {
        _obstacleConfiguration.scaleTouch.x = value;
    }

    public void SetScaleYTouch(float value)
    {
        _obstacleConfiguration.scaleTouch.y = value;
    }
}
