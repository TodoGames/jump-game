﻿using UnityEngine;
using UnityEngine.UI;

public class UI_DifficultyConfiguration : MonoBehaviour
{
    [SerializeField] private DroppableItemsConfiguration items;
    [SerializeField] private DroppableItemsConfiguration skills;
    [SerializeField] private DroppableItemsConfiguration obstacles;
    [SerializeField] private Text information;
    [Header("Sliders")]
    [SerializeField] private string idHealth;
    [SerializeField] private Slider _sliderProbHealth;
    [SerializeField] private string idSkill;
    [SerializeField] private Slider _sliderProbSkills;
    [SerializeField] private string idObstacle;
    [SerializeField] private Slider _sliderProbObstacle;
    [SerializeField] private string idIce;
    [SerializeField] private Slider _sliderProbIce;
    [SerializeField] private string keyScore;
    [SerializeField] private Slider _sliderMAxScore;

    private void Start()
    {
        SliderConfigurations();
        SetInitialValues();
    }

    private void SliderConfigurations()
    {
        _sliderProbHealth.onValueChanged.AddListener(SetHealth);
        _sliderProbSkills.onValueChanged.AddListener(SetSkill);
        _sliderProbObstacle.onValueChanged.AddListener(SetObstacle);
        _sliderProbIce.onValueChanged.AddListener(SetIce);
        _sliderMAxScore.onValueChanged.AddListener(SetMaxScore);
    }

    public void SetInitialValues()
    {
        _sliderProbSkills.value = skills.GetValue(idSkill);
        _sliderProbObstacle.value = obstacles.GetValue(idObstacle);
        _sliderProbIce.value = obstacles.GetValue(idIce);
        _sliderMAxScore.value = PlayerPrefs.GetInt(keyScore, 500);
        _sliderProbHealth.value = items.GetValue(idHealth);
    }

    private void Update()
    {
        information.text = string.Format("PROBABILIDADES\nVIDAS: {0}\nHABILIDAD: {1}\nOBSTACULO: {2}\nCONGELAS: {3}\nCANTIDAD PUNTOS MINIMO: {4}",
            _sliderProbHealth.value, _sliderProbSkills.value, _sliderProbObstacle.value, _sliderProbIce.value, _sliderMAxScore.value);
    }

    public void SetHealth(float value)
    {
        items.ChangeValueItem(idHealth, value);
    }

    public void SetSkill(float value)
    {
        skills.ChangeValueItem(idSkill, value);
    }

    public void SetObstacle(float value)
    {
        obstacles.ChangeValueItem(idObstacle, value);
    }
    public void SetIce(float value)
    {
        obstacles.ChangeValueItem(idIce, value);
    }
    public void SetMaxScore(float value)
    {
        PlayerPrefs.SetInt(keyScore, (int)value);
    }
}