using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigurationsController : MonoBehaviour
{
    [Header("Platforms Configuration")]
    [SerializeField] private PlatformConfigurationSO noramlPlatform;
    [SerializeField] private PlatformConfigurationSO jumpPlatform;
    [SerializeField] private PlatformConfigurationSO leftPlatform;
    [SerializeField] private PlatformConfigurationSO rigthPlatform;
    [SerializeField] private ConfigurationOfPlatformToCreate configurationGeneralPlatform;
    [Header("Obstacles Configuration")]
    [SerializeField] private ObstacleConfigurationSO redObstacle;
    [SerializeField] private ObstacleConfigurationSO blueObstacle;
    [Header("IDs")]
    [SerializeField] private string idSkill;
    [SerializeField] private string idObstacle;
    [SerializeField] private string idIce;
    [SerializeField] private string idHealth;
    [Header("Difficul Easy Configuration")]
    [SerializeField] private DroppableItemsConfiguration itemDropEasy;
    [SerializeField] private DroppableItemsConfiguration skillDropEasy;
    [SerializeField] private DroppableItemsConfiguration obstacleDropEasy;
    [Header("Difficul Medium Configuration")]
    [SerializeField] private DroppableItemsConfiguration itemDropMedium;
    [SerializeField] private DroppableItemsConfiguration skillDropMedium;
    [SerializeField] private DroppableItemsConfiguration obstacleDropMedium;
    [Header("Difficul Hard Configuration")]
    [SerializeField] private DroppableItemsConfiguration itemDropHard;
    [SerializeField] private DroppableItemsConfiguration skillDropHard;
    [SerializeField] private DroppableItemsConfiguration obstacleDropHard;
    private string KeyEasy = "EASY_POINTS";
    private string KeyMedium = "MEDIUM_POINTS";
    private string KeyHard = "HARD_POINTS";
    [Header("Player Configuration")]
    [SerializeField] private PlayerConfigurationSO playerConfigurationSO;
    [Header("Difficult Version 2")]
    [SerializeField] private QuatityItemToSpawn quantityPowerUpToSpawn;
    [SerializeField] private QuatityItemToSpawn quantityHealthToSpawn;
    [SerializeField] private QuatityItemToSpawn quantityDamgeToSpawn;
    [SerializeField] private QuatityItemToSpawn quantityIceToSpawn;

    public string GetConfigurationOnJson()
    {
        Dictionary<string, string> configuration = new Dictionary<string, string>();

        configuration.Add("PlatformNormal", GetStructPlatformToJson(noramlPlatform, "Normal"));
        configuration.Add("PlatformLeft", GetStructPlatformToJson(leftPlatform, "Diagonal Izquierda"));
        configuration.Add("PlatformRight", GetStructPlatformToJson(rigthPlatform, "Diagonal Derecha"));
        configuration.Add("PlatformJump", GetStructPlatformToJson(jumpPlatform, "Super Jump"));

        configuration.Add("ObstacleRED", GetStructObstacleToJson(redObstacle));
        configuration.Add("ObstacleBLUE", GetStructObstacleToJson(blueObstacle));

        configuration.Add("Player", GetStructPlayerToJson(playerConfigurationSO));

        configuration.Add("Easy", GetStructDifficultyToJson(itemDropEasy, skillDropEasy, obstacleDropEasy, KeyEasy));
        configuration.Add("Medium", GetStructDifficultyToJson(itemDropMedium, skillDropMedium, obstacleDropMedium, KeyMedium));
        configuration.Add("Hard", GetStructDifficultyToJson(itemDropHard, skillDropHard, obstacleDropHard, KeyHard));

        configuration.Add("PowerUpToSpawn", GetStructDifficultVersion2(quantityPowerUpToSpawn));
        configuration.Add("HealthToSpawn", GetStructDifficultVersion2(quantityHealthToSpawn));
        configuration.Add("DamageToSpawn", GetStructDifficultVersion2(quantityDamgeToSpawn));
        configuration.Add("IceToSpawn", GetStructDifficultVersion2(quantityIceToSpawn));
        return GetInformation(configuration);
    }

    private string GetInformation(Dictionary<string, string> configuration)
    {
        StructListConfiguration structListConfiguration = new StructListConfiguration();
        structListConfiguration._structConfigurations = new List<StructConfigurations>();
        foreach (var item in configuration)
        {
            StructConfigurations structConfigurations = new StructConfigurations();
            structConfigurations.idConfiguration = item.Key;
            structConfigurations.valueConfiguration = item.Value;
            structListConfiguration._structConfigurations.Add(structConfigurations);
            Debug.Log(item.Key + " " + item.Value);
        }
        string information = JsonUtility.ToJson(structListConfiguration);
        return information;
    }

    public void SetConfiguration(string information)
    {
        Dictionary<string, string> newInformation = new Dictionary<string, string>();
        StructListConfiguration structListConfiguration = JsonUtility.FromJson<StructListConfiguration>(information);
        foreach (var item in structListConfiguration._structConfigurations)
        {
            newInformation.Add(item.idConfiguration, item.valueConfiguration);
        }

        SetStructPlatform(noramlPlatform, "Normal", newInformation["PlatformNormal"]);
        SetStructPlatform(leftPlatform, "Diagonal Izquierda", newInformation["PlatformLeft"]);
        SetStructPlatform(rigthPlatform, "Diagonal Derecha", newInformation["PlatformRight"]);
        SetStructPlatform(jumpPlatform, "Super Jump", newInformation["PlatformJump"]);

        SetStructObstacle(redObstacle, newInformation["ObstacleRED"]);
        SetStructObstacle(blueObstacle, newInformation["ObstacleBLUE"]);

        SetStructPlayer(playerConfigurationSO, newInformation["Player"]);

        SetStructDiffuculty(itemDropEasy, skillDropEasy, obstacleDropEasy, KeyEasy, newInformation["Easy"]);
        SetStructDiffuculty(itemDropMedium, skillDropMedium, obstacleDropMedium, KeyMedium, newInformation["Medium"]);
        SetStructDiffuculty(itemDropHard, skillDropHard, obstacleDropHard, KeyHard, newInformation["Hard"]);

        SetStructQuantityToSpawn(quantityPowerUpToSpawn, newInformation["PowerUpToSpawn"]);
        SetStructQuantityToSpawn(quantityHealthToSpawn, newInformation["HealthToSpawn"]);
        SetStructQuantityToSpawn(quantityDamgeToSpawn, newInformation["DamageToSpawn"]);
        SetStructQuantityToSpawn(quantityIceToSpawn, newInformation["IceToSpawn"]);
    }

    private void SetStructPlatform(PlatformConfigurationSO _platformConfigurationSO, string _idPlatform, string jsonInformation)
    {
        StructPlatformConfiguration structPlatformConfiguration = JsonUtility.FromJson<StructPlatformConfiguration>(jsonInformation);
        _platformConfigurationSO._platformScale.x = structPlatformConfiguration._scalePlatform.x;
        _platformConfigurationSO._platformScale.y = structPlatformConfiguration._scalePlatform.y;
        _platformConfigurationSO._touchScale.x = structPlatformConfiguration._scaleTouch.x;
        _platformConfigurationSO._touchScale.y = structPlatformConfiguration._scaleTouch.y;
        _platformConfigurationSO._forceToJump = structPlatformConfiguration._forceJump;
        _platformConfigurationSO._forceHorizontal = structPlatformConfiguration._forceHorizontal;
        _platformConfigurationSO._maxHealth = structPlatformConfiguration._maxHealth;
        configurationGeneralPlatform.SetCoolDown(_idPlatform, structPlatformConfiguration._coolDown);
        configurationGeneralPlatform.SetMaxItemsToActive(_idPlatform, (int)structPlatformConfiguration._MaxItems);
    }

    private void SetStructObstacle(ObstacleConfigurationSO _obstacleConfiguration, string jsonInformation)
    {
        StructObstacleConfiguration structObstacleConfiguration = JsonUtility.FromJson<StructObstacleConfiguration>(jsonInformation);
        _obstacleConfiguration.scaleObstacle.y = structObstacleConfiguration._scalePlatform.y;
        _obstacleConfiguration.scaleObstacle.x = structObstacleConfiguration._scalePlatform.x;
        _obstacleConfiguration.scaleTouch.x = structObstacleConfiguration._scaleTouch.x;
        _obstacleConfiguration.scaleTouch.y = structObstacleConfiguration._scaleTouch.y;
    }

    private void SetStructPlayer(PlayerConfigurationSO playerConfigurationSO, string jsonInformation)
    {
        StructPlayerConfiguration structPlayerConfiguration = JsonUtility.FromJson<StructPlayerConfiguration>(jsonInformation);
        playerConfigurationSO.maxFall = structPlayerConfiguration._maxFall;
        playerConfigurationSO.initialHealth = structPlayerConfiguration._initialHealth;
        playerConfigurationSO.maxJump = structPlayerConfiguration._maxJump;
    }

    private void SetStructDiffuculty(DroppableItemsConfiguration items, DroppableItemsConfiguration skills, DroppableItemsConfiguration obstacles, string keyScore, string jsonInformation)
    {
        StructDifficulty structDifficulty = JsonUtility.FromJson<StructDifficulty>(jsonInformation);
        skills.ChangeValueItem(idSkill, structDifficulty._propSkill);
        obstacles.ChangeValueItem(idObstacle, structDifficulty._propObstacle);
        obstacles.ChangeValueItem(idIce, structDifficulty._propIce);
        PlayerPrefs.SetInt(keyScore, (int)structDifficulty._MaxScore);
        items.ChangeValueItem(idHealth, structDifficulty._propHealth);
    }

    private void SetStructQuantityToSpawn(QuatityItemToSpawn quatityItemToSpawn, string jsonInformation)
    {
        StructQuantityToSpawn structQuantityToSpawn = JsonUtility.FromJson<StructQuantityToSpawn>(jsonInformation);
        quatityItemToSpawn.MaxItemsToSpawn = structQuantityToSpawn._maxItems;
        quatityItemToSpawn.MinItemsToSpawn = structQuantityToSpawn._minItems;
        quatityItemToSpawn.ProbabilityToSpawn = structQuantityToSpawn._probability;
    }

    private string GetStructPlatformToJson(PlatformConfigurationSO _platformConfigurationSO, string _idPlatform)
    {
        StructPlatformConfiguration structPlatformConfiguration = new StructPlatformConfiguration();
        structPlatformConfiguration._scalePlatform.x = _platformConfigurationSO._platformScale.x;
        structPlatformConfiguration._scalePlatform.y = _platformConfigurationSO._platformScale.y;
        structPlatformConfiguration._scaleTouch.x = _platformConfigurationSO._touchScale.x;
        structPlatformConfiguration._scaleTouch.y = _platformConfigurationSO._touchScale.y;
        structPlatformConfiguration._forceJump = _platformConfigurationSO._forceToJump;
        structPlatformConfiguration._forceHorizontal = _platformConfigurationSO._forceHorizontal;
        structPlatformConfiguration._maxHealth = _platformConfigurationSO._maxHealth;
        structPlatformConfiguration._coolDown = configurationGeneralPlatform.GetCooldown(_idPlatform);
        structPlatformConfiguration._MaxItems = configurationGeneralPlatform.GetMaxItemsToActive(_idPlatform);
        string info = JsonUtility.ToJson(structPlatformConfiguration);
        return info;
    }

    private string GetStructObstacleToJson(ObstacleConfigurationSO _obstacleConfiguration)
    {
        StructObstacleConfiguration structObstacleConfiguration = new StructObstacleConfiguration();
        structObstacleConfiguration._scalePlatform.y = _obstacleConfiguration.scaleObstacle.y;
        structObstacleConfiguration._scalePlatform.x = _obstacleConfiguration.scaleObstacle.x;
        structObstacleConfiguration._scaleTouch.x = _obstacleConfiguration.scaleTouch.x;
        structObstacleConfiguration._scaleTouch.y = _obstacleConfiguration.scaleTouch.y;
        string info = JsonUtility.ToJson(structObstacleConfiguration);
        return info;
    }

    private string GetStructPlayerToJson(PlayerConfigurationSO playerConfigurationSO)
    {
        StructPlayerConfiguration structPlayerConfiguration = new StructPlayerConfiguration();
        structPlayerConfiguration._maxFall = playerConfigurationSO.maxFall;
        structPlayerConfiguration._initialHealth = playerConfigurationSO.initialHealth;
        structPlayerConfiguration._maxJump = playerConfigurationSO.maxJump;
        string info = JsonUtility.ToJson(structPlayerConfiguration);
        return info;
    }

    private string GetStructDifficultyToJson(DroppableItemsConfiguration items, DroppableItemsConfiguration skills, DroppableItemsConfiguration obstacles, string keyScore)
    {
        StructDifficulty structDifficulty = new StructDifficulty();
        structDifficulty._propSkill = skills.GetValue(idSkill);
        structDifficulty._propObstacle = obstacles.GetValue(idObstacle);
        structDifficulty._propIce = obstacles.GetValue(idIce);
        structDifficulty._MaxScore = PlayerPrefs.GetInt(keyScore, 500);
        structDifficulty._propHealth = items.GetValue(idHealth);
        string info = JsonUtility.ToJson(structDifficulty);
        return info;
    }

    private string GetStructDifficultVersion2(QuatityItemToSpawn quatityItemToSpawn)
    {
        StructQuantityToSpawn structQuantityToSpawn = new StructQuantityToSpawn();
        structQuantityToSpawn._maxItems = quatityItemToSpawn.MaxItemsToSpawn;
        structQuantityToSpawn._minItems = quatityItemToSpawn.MinItemsToSpawn;
        structQuantityToSpawn._probability = quatityItemToSpawn.ProbabilityToSpawn;
        string info = JsonUtility.ToJson(structQuantityToSpawn);
        return info;
    }
}
