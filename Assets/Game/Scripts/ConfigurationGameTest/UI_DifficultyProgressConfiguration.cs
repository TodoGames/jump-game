using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DifficultyProgressConfiguration : MonoBehaviour
{
    [SerializeField] private QuatityItemToSpawn _quatityItemToSpawn;
    [SerializeField] private Text _information;
    [SerializeField] private Slider _sliderMaxItemsToSpawn;
    [SerializeField] private Slider _sliderMinItemsToSpawn;
    [SerializeField] private Slider _sliderProbabilityToSpawn;

    private void Start()
    {
        SliderConfigurations();
        SetInitialValues();
    }

    private void Update()
    {
        _information.text = string.Format("VALORES\nMax Items: {0}\nMin Items: {1}\nProbabilidad: {2}",
            _sliderMaxItemsToSpawn.value, _sliderMinItemsToSpawn.value, _sliderProbabilityToSpawn.value);        
    }

    private void SliderConfigurations()
    {
        _sliderMaxItemsToSpawn.onValueChanged.AddListener(SetMaxItems);
        _sliderMinItemsToSpawn.onValueChanged.AddListener(SetMinItems);
        _sliderProbabilityToSpawn.onValueChanged.AddListener(SetProbabilityToSpawn);
    }

    public void SetInitialValues()
    {
        _sliderMaxItemsToSpawn.value = _quatityItemToSpawn.MaxItemsToSpawn;
        _sliderMinItemsToSpawn.value = _quatityItemToSpawn.MinItemsToSpawn;
        _sliderProbabilityToSpawn.value = _quatityItemToSpawn.ProbabilityToSpawn;
    }

    public void SetMaxItems(float value)
    {
        _quatityItemToSpawn.MaxItemsToSpawn = (int)value;
    }

    public void SetMinItems(float value)
    {
        _quatityItemToSpawn.MinItemsToSpawn = (int)value;
    }

    public void SetProbabilityToSpawn(float value)
    {
        _quatityItemToSpawn.ProbabilityToSpawn = value;
    }
}
