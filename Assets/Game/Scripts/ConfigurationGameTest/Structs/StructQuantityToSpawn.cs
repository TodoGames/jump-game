﻿using System;

[Serializable]
public struct StructQuantityToSpawn
{
    public int _maxItems;
    public int _minItems;
    public float _probability;
}