﻿using System;
using UnityEngine;

[Serializable]
public struct StructObstacleConfiguration
{
    public Vector3 _scalePlatform;
    public Vector3 _scaleTouch;
}
