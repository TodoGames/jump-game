﻿using System;

[Serializable]
public struct StructPlayerConfiguration
{
    public float _maxJump;
    public float _maxFall;
    public int _initialHealth;
}
