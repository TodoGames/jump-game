﻿using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public struct StructListConfiguration
{
    public List<StructConfigurations> _structConfigurations;
}