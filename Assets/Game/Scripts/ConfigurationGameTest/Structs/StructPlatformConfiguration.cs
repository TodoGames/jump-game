﻿using System;
using UnityEngine;

[Serializable]
public struct StructPlatformConfiguration
{
    public Vector3 _scalePlatform;
    public Vector3 _scaleTouch;
    public float _forceJump;
    public float _forceHorizontal;
    public int _maxHealth;
    public float _coolDown;
    public float _MaxItems;
}
