﻿using System;

[Serializable]
public struct StructDifficulty
{
    public float _propHealth;
    public float _propSkill;
    public float _propObstacle;
    public float _propIce;
    public float _MaxScore;
}
