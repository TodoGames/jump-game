﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMobilePlatform : MobilePlatform
{
    [Header("Objects to Hide")]
    [SerializeField] private LayerMask _layerMaskToHide;
    [SerializeField] private BoxCollider2D _boxColliderForOverlap;
    [SerializeField] private Collider2D _collider2DToDetect;
    [SerializeField] private GameObject _killPlatform;
    [Header("Color Information")]
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Color _normalColor;
    [SerializeField] private Color _spawnAvailableColor = Color.green;
    [SerializeField] private Color _spawnNotAvailableColor = Color.red;
    private bool _isFirstSpawn = true;
    private bool _isAvailableSpace;
    private ContactFilter2D _contactFilter2D;
    private Collider2D[] _collidersResult;

    public Action _actionOnClickStart;
    public Action _actionOnClickCancel;
    public Action _actionOnClickEnd;

    protected override void Start()
    {
        base.Start();
        Filter2DInitializer();
    }

    private void Filter2DInitializer()
    {
        _contactFilter2D = new ContactFilter2D();
        _contactFilter2D.useTriggers = true;
        _contactFilter2D.useLayerMask = true;
        _contactFilter2D.layerMask = _layerMaskToHide;
        _collidersResult = new Collider2D[10];
    }

    protected override void Update()
    {
        CheckAvailableSpace();
        base.Update();
    }

    private void CheckAvailableSpace()
    {
        if (_isFirstSpawn)
        {
            int amountCollisions = _boxColliderForOverlap.OverlapCollider(_contactFilter2D, _collidersResult);
            _isAvailableSpace = amountCollisions <= 0;

            if (_isAvailableSpace)
            {
                _spriteRenderer.color = _spawnAvailableColor;
            }
            else
            {
                _spriteRenderer.color = _spawnNotAvailableColor;
            }
        }
        else
        {
            _spriteRenderer.color = _normalColor;
        }
    }

    public override void OnClickMouseDown(int indexFinger)
    {
        base.OnClickMouseDown(indexFinger);
        _isAvailableSpace = true;
        _actionOnClickStart?.Invoke();
        _collider2DToDetect.enabled = false;
    }

    public override void OnClickMouseUp()
    {
        base.OnClickMouseUp();

        if (_isFirstSpawn)
        {
            if (_isAvailableSpace)
            {
                _isFirstSpawn = false;
                _actionOnClickEnd?.Invoke();
            }
            else
            {
                _actionOnClickCancel?.Invoke();
            }
            _killPlatform.SetActive(true);
        }
        else
        {
            _actionOnClickEnd?.Invoke();
        }
        _collider2DToDetect.enabled = true;
    }

    protected override void OnDestroy()
    {
        _actionOnClickEnd?.Invoke();
    }
}