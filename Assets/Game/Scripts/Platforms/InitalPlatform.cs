﻿using UnityEngine;
using UnityEngine.UI;

public class InitalPlatform : MonoBehaviour
{
    [SerializeField] private float _timeToHidePlatform;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Text _currentTimeText;

    private float _currentTimeToHide;

    private void Start()
    {
        Initializer();
        ActiveInitialPlatform();
    }

    private void Update()
    {
        if(_currentTimeToHide <= 0)
        {
            DesactiveInitialPlatform();            
        }
        else
        {
            _currentTimeToHide -= Time.deltaTime;
        }
        UpdateCurrentTimeText();
    }

    private void Initializer()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void ActiveInitialPlatform()
    {
        _currentTimeToHide = _timeToHidePlatform;
        Color currentColor = _spriteRenderer.color;
        currentColor.a = 1;
        _spriteRenderer.color = currentColor;
        _currentTimeText.gameObject.SetActive(true);
    }

    public void DesactiveInitialPlatform()
    {
        _currentTimeToHide = 0;
        Color currentColor = _spriteRenderer.color;
        currentColor.a = 0;
        _spriteRenderer.color = currentColor;
        _currentTimeText.gameObject.SetActive(false);
        Destroy(gameObject);
    }

    private void UpdateCurrentTimeText()
    {
        _currentTimeText.text = Mathf.RoundToInt(_currentTimeToHide).ToString();
    }
}