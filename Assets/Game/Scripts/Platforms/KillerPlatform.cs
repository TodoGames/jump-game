using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerPlatform : MonoBehaviour
{
    [SerializeField] private string _tagToKillPlatform;
    [SerializeField] private GameObject _platformToDestroy;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(_tagToKillPlatform))
        {
            Destroy(_platformToDestroy);
        }
    }
}
