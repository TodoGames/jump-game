﻿using System.Collections;
using UnityEngine;

public class GameOverPlatform : MonoBehaviour, IJumpRigidbody2D
{
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private int _amountHealthToJump;
    [SerializeField] private PlatformConfigurationSO _platformConfigurationSO;
    [SerializeField] private float powerJump = 1;
    private Collider2D currentCollider;

    public void DoJump(Rigidbody2D _rigidbody2D)
    {
        var health = _rigidbody2D.GetComponent<AbstractHealth>();
        if (health != null)
        {
            if (health.GetCurrentHealth() > _amountHealthToJump)
            {
                _rigidbody2D.velocity = Vector2.zero;
                Vector2 directionToJump = Vector2.up;
                directionToJump.x = Random.Range(-0.9f, 0.9f);
                directionToJump.y *= (_platformConfigurationSO._forceToJump * 0.85f);
                directionToJump.x *= (_platformConfigurationSO._forceHorizontal * 0.85f);
                _rigidbody2D.AddForce(directionToJump*powerJump, ForceMode2D.Impulse);
                currentCollider = _rigidbody2D.GetComponent<Collider2D>();
                DoChangeColor();
            }
            health.DecrementHealth(_amountHealthToJump);
        }
    }

    private void DoChangeColor()
    {
        _spriteRenderer.color = Color.green;
        currentCollider.enabled = false;
        StartCoroutine(ReturnFirstColor());
    }

    IEnumerator ReturnFirstColor()
    {
        yield return new WaitForSeconds(1f);
        _spriteRenderer.color = Color.red;
        currentCollider.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var itemCollision = collision.GetComponent<AbstractItem>();
        if(itemCollision != null)
        {
            //Destroy(itemCollision.gameObject);
            itemCollision.gameObject.SetActive(false);
        }
    }
}