﻿using UnityEngine;

[CreateAssetMenu(fileName = "Platform Configuration", menuName ="Configurations/PlatformConfig")]
public class PlatformConfigurationSO : ScriptableObject
{
    public Vector2 _touchScale;
    public Vector3 _platformScale;
    public float _forceToJump;
    public float _forceHorizontal;
    public int _maxHealth;
}