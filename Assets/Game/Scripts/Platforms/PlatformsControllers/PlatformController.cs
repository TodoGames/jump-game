﻿using UnityEngine;

public class PlatformController : MonoBehaviour
{
    [SerializeField] protected PlatformConfigurationSO _platformConfigurationSO;
    [SerializeField] protected ImpulseJumpRigidbody2D _impulseJumpRigidbody2D;
    [SerializeField] protected Transform _platfrom;
    protected CreatePlatformOnClick _createPlatformOnClick;
    protected int _currentIndexFinger;
    protected virtual void Awake()
    {
        _platfrom.localScale = _platformConfigurationSO._platformScale;
        _impulseJumpRigidbody2D.Configuration(_platformConfigurationSO._forceToJump, _platformConfigurationSO._forceHorizontal);
    }

    public virtual void SetInitalSpawnConfiguration(CreatePlatformOnClick createPlatformOnClick, int indexFinger)
    {
        _createPlatformOnClick = createPlatformOnClick;
        _currentIndexFinger = indexFinger;
    }
}
