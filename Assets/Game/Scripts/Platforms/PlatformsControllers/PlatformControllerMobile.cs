﻿using UnityEngine;

public class PlatformControllerMobile : PlatformController
{
    [SerializeField] private MobilePlatform _mobilePlatform;
    [SerializeField] protected AbstractHealth _abstractHealth;
    [SerializeField] private SpawnMobilePlatform _spawnMobilePlatform;
    protected override void Awake()
    {
        base.Awake();
        if (_mobilePlatform) _mobilePlatform.SetColliderSizeForTouch(_platformConfigurationSO._touchScale);
        _abstractHealth.SetMaxHealth(_platformConfigurationSO._maxHealth);
        _abstractHealth.SetCurrentHealth(_platformConfigurationSO._maxHealth);
    }

    public override void SetInitalSpawnConfiguration(CreatePlatformOnClick createPlatformOnClick, int indexFinger)
    {
        base.SetInitalSpawnConfiguration(createPlatformOnClick, indexFinger);
        _spawnMobilePlatform.OnClickMouseDown(indexFinger);
        _spawnMobilePlatform._actionOnClickCancel += _createPlatformOnClick.CancelSpawn;
        _spawnMobilePlatform._actionOnClickCancel += ResetEventsSpawnMobilePlatform;
        _spawnMobilePlatform._actionOnClickEnd += _createPlatformOnClick.EndSpawnPlatform;
        _spawnMobilePlatform._actionOnClickEnd += ResetEventsSpawnMobilePlatform;
    }

    private void ResetEventsSpawnMobilePlatform()
    {
        _spawnMobilePlatform._actionOnClickCancel -= _createPlatformOnClick.CancelSpawn;
        _spawnMobilePlatform._actionOnClickCancel -= ResetEventsSpawnMobilePlatform;
        _spawnMobilePlatform._actionOnClickEnd -= _createPlatformOnClick.EndSpawnPlatform;
        _spawnMobilePlatform._actionOnClickEnd -= ResetEventsSpawnMobilePlatform;
    }
}