using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawnToSpawn : MonoBehaviour
{
    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private float _minValueToHorizontal;
    [SerializeField] private GameObject _spawnHorizontal;
    private Vector3 _startScreenPosition;
    private Vector3 _endScreenPosition;
    private bool _drawnLine;

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _startScreenPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            _drawnLine = true;
            _lineRenderer.gameObject.SetActive(true);
        }

        if (Input.GetMouseButtonUp(0))
        {
            _endScreenPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            _endScreenPosition.z = 0;
            Vector2 startWorldPosition = Camera.main.ViewportToWorldPoint(_startScreenPosition);
            Vector2 endWorldPosition = Camera.main.ViewportToWorldPoint(_endScreenPosition);
            Vector2 centerPosition = (startWorldPosition + endWorldPosition) / 2f;

            Vector3 direction = endWorldPosition - startWorldPosition;
            direction.Normalize();

            GameObject gameobjectSpawned = Instantiate(_spawnHorizontal, centerPosition, Quaternion.identity);
           
            Vector3 newScale = Vector3.one;
            newScale.x *= Vector3.Distance(startWorldPosition, endWorldPosition);
            //gameobjectSpawned.transform.localScale = newScale;
            gameobjectSpawned.GetComponent<SpriteRenderer>().size = newScale;
            gameobjectSpawned.GetComponent<BoxCollider2D>().size = newScale;
            Debug.Log(direction);
            float angle_Z = Mathf.Atan2(direction.y, direction.x)*Mathf.Rad2Deg;
            gameobjectSpawned.transform.rotation = Quaternion.Euler(0f, 0f, angle_Z);

            PlatformEffector2D platformEffector2D = gameobjectSpawned.GetComponent<PlatformEffector2D>();
            if(platformEffector2D != null)
            {
                Vector2 directionObject = gameobjectSpawned.transform.up;
                if(directionObject.y < 0)
                {
                    platformEffector2D.rotationalOffset = 180;
                    gameobjectSpawned.GetComponent<SpriteRenderer>().flipY = true;
                }
            }

            Destroy(gameobjectSpawned, 5f);
            _lineRenderer.gameObject.SetActive(false);
            _drawnLine = false;
        }

        if (_drawnLine)
        {
            Vector2 newworldPosition = Camera.main.ViewportToWorldPoint(_startScreenPosition);
            _lineRenderer.SetPosition(0, newworldPosition);
            newworldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _lineRenderer.SetPosition(1, newworldPosition);
        }
    }
}
