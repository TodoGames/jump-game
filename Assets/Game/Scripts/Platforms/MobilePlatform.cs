using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider2D))]
public class MobilePlatform : MonoBehaviour
{
    [Header("Settings Touch")]
    [SerializeField] protected Transform _tranformObjectToMove;
    [SerializeField] protected BoxCollider2D _boxCollider2DToTouch;
    [SerializeField] protected Vector2 _colliderSizeForTouch;
    protected bool _isOnClick;
    protected int _indexFinger = -1;

    protected virtual void Start()
    {
        if (_boxCollider2DToTouch == null) _boxCollider2DToTouch = GetComponent<BoxCollider2D>();
        _boxCollider2DToTouch.size = _colliderSizeForTouch;
    }

    protected virtual void Update()
    {
        if (!_isOnClick)
        {
            return;
        }

        if (_indexFinger >= Input.touchCount)
        {
            OnClickMouseUp();
            return;
        }

        OnClickMouseDrag();

        if (Input.GetTouch(_indexFinger).phase == TouchPhase.Ended || Input.GetTouch(_indexFinger).phase == TouchPhase.Canceled)
        {
            OnClickMouseUp();
        }
    }

    public void SetColliderSizeForTouch(Vector2 colliderSizeForTouch)
    {
        _colliderSizeForTouch = colliderSizeForTouch;
        _boxCollider2DToTouch.size = _colliderSizeForTouch;
    }

    public virtual void OnClickMouseDown(int indexFinger)
    {
        if (_isOnClick)
        {
            return;
        }
        _isOnClick = true;
        _indexFinger = indexFinger;
    }

    public virtual void OnClickMouseDrag()
    {
        Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(_indexFinger).position);
        newPosition.z = 0;
        _tranformObjectToMove.position = newPosition;
    }

    public virtual void OnClickMouseUp()
    {
        _isOnClick = false;
        _indexFinger = -1;
    }

    protected virtual void OnDestroy()   { }
}
