﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class TestLogin : MonoBehaviour
{
    public GameObject activeEndLogin;
    private void Start()
    {
        AutoLogin();
    }
    void AutoLogin()
    {
        var request = new LoginWithPlayFabRequest()
        {
            Username = "Test",
            Password = "123456789"
        };
        PlayFabClientAPI.LoginWithPlayFab(request, OnLoginSucces, Error);
    }

    void OnLoginSucces(LoginResult loginResult)
    {
        PlayerPrefs.SetString("ID", loginResult.PlayFabId);
        //Debug.Log("<Color=green>Login succes</Color>");
        activeEndLogin.SetActive(true);
    }

    void Error(PlayFabError error)
    {
        Debug.LogError(error.ErrorMessage);
        var request = new RegisterPlayFabUserRequest()
        {
            Email = "Test_@mail.com",
            Username = "Test",
            Password = "123456789"
        };

        PlayFabClientAPI.RegisterPlayFabUser(
            request,
            result => { AutoLogin(); },
            errorRegister => { Debug.LogError(errorRegister.ErrorMessage); });
    }
}
