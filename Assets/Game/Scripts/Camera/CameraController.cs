using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _player;
    [SerializeField] private Vector3 _offesetPosition;
    [SerializeField] private float _smoothTimeToMove;
    private Vector3 _currentVelocityMovement;

    private void FixedUpdate()
    {
        if(_player == null)
        {
            return;
        }
        if (_player.position.y >= GetCurrentYPosition())
        {
            Vector3 newPosition = transform.position;
            newPosition.y += Mathf.Abs(_player.position.y - GetCurrentYPosition());
            if (newPosition.y > transform.position.y)
            {
                transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref _currentVelocityMovement, _smoothTimeToMove);
            }
        }
    }

    public float GetCurrentYPosition()
    {
        return transform.position.y + _offesetPosition.y;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Vector3 limitPosition = transform.position +_offesetPosition;
        Vector3 point1,point2;
        point1 = limitPosition + (Vector3.right * 5);
        point2 = limitPosition + (Vector3.left * 5);
        Gizmos.DrawLine(point1, point2);
    }
}
