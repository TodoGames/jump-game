﻿using UnityEngine;

[CreateAssetMenu(fileName = "Quatity Items", menuName = "Configurations/Quatity Items To spawn")]
public class QuatityItemToSpawn : ScriptableObject
{
    [SerializeField] private AbstractItem _itemToSpawn;
    [SerializeField] private int _maxItemsToSpawn;
    [SerializeField] private int _minItemsToSpawn;
    [SerializeField] private float _probabilityToSpawn;

    public int MaxItemsToSpawn { get { return _maxItemsToSpawn; } set { _maxItemsToSpawn = value; } }
    public int MinItemsToSpawn { get { return _minItemsToSpawn; } set { _minItemsToSpawn = value; } }
    public float ProbabilityToSpawn { get { return _probabilityToSpawn; } set { _probabilityToSpawn = value; } }

    public int GetValueMinToMax(float percent)
    {
        return Mathf.RoundToInt(Mathf.Lerp(_minItemsToSpawn, _maxItemsToSpawn, percent));
    }

    public int GetValueMaxToMin(float percent)
    {
        return Mathf.RoundToInt(Mathf.Lerp(_maxItemsToSpawn, _minItemsToSpawn, percent));
    }

    public AbstractItem GetItemToSpawn()
    {
        return _itemToSpawn;
    }
}