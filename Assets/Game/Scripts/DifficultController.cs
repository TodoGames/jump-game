using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelEditor_Positions;

public partial class DifficultController : MonoBehaviour
{
    [Header("Items To Spawn")]
    [SerializeField] private QuatityItemToSpawn _quantityPowerJump;
    [SerializeField] private QuatityItemToSpawn _quantityHealth;
    [SerializeField] private QuatityItemToSpawn _quantityIce;
    [SerializeField] private QuatityItemToSpawn _quantityDamage;
    [Header("Distances")]
    [SerializeField] private Transform _player;
    [SerializeField] private float _minDistanceToSpawn;
    [SerializeField] private int _amountPoints;
    [SerializeField] private float _distanceAvailableToSpawn;
    [SerializeField] private float _minDistanceToMoveY;
    [Header("Difficult")]
    [SerializeField] private Score _score;
    [SerializeField] public float _maxTimeScale;
    [Header("Difficult By Pool Object")]
    [SerializeField] private PoolObjectPositioner _poolObjectPositioner;
    [SerializeField] private TypesOfDifficultySO _typesOfDifficultySO;
    private float _distanceBetweenSpawns;
    private List<Vector3> _positionsToSpawn;

    void Start()
    {
        CreateSpawnPoints();
        _maxTimeScale = PlayerPrefs.GetFloat("MaxTimeScale", 1);
    }

    private void CreateSpawnPoints()
    {
        _distanceBetweenSpawns = _distanceAvailableToSpawn / (_amountPoints - 1);
        _positionsToSpawn = new List<Vector3>();
        Vector3 currnetPoint = Vector3.zero;
        currnetPoint.x -= (_distanceAvailableToSpawn / 2f);
        for (int i = 0; i < _amountPoints; i++)
        {
            _positionsToSpawn.Add(currnetPoint + (Vector3.right * _distanceBetweenSpawns * i));
        }
    }

    void Update()
    {
        if (!_player)
        {
            Time.timeScale = 1f;
            return;
        }

        Vector3 currentPlayerPosition = _player.position;
        currentPlayerPosition.x = transform.position.x;
        if (Vector3.Distance(transform.position, currentPlayerPosition) < _minDistanceToSpawn)
        {
            if(_poolObjectPositioner)
            {
                string currentTypeOfDifficult = _typesOfDifficultySO.GetNameByPercent(_score.GetCurrentScoreProgress());
                _poolObjectPositioner.SetTypeOfDifficulty(currentTypeOfDifficult);
            }
            else
            {
                StartSpawnItems();
            }
        }
        Time.timeScale = Mathf.Lerp(1f, _maxTimeScale, _score.GetCurrentScoreProgress());
    }

    public void StartSpawnItems()
    {
        int currentNumberOfItems = _quantityPowerJump.GetValueMaxToMin(_score.GetCurrentScoreProgress());
        int currentNumberOfHealths = _quantityHealth.GetValueMaxToMin(_score.GetCurrentScoreProgress());
        int currentNumberOfDamage = _quantityDamage.GetValueMinToMax(_score.GetCurrentScoreProgress());
        int currentNumberOfIce = _quantityIce.GetValueMinToMax(_score.GetCurrentScoreProgress());

        SpawnItem(_quantityDamage, currentNumberOfDamage);
        SpawnItem(_quantityPowerJump, currentNumberOfItems);
        SpawnItem(_quantityDamage, currentNumberOfDamage);
        SpawnItem(_quantityPowerJump, currentNumberOfItems);
        SpawnItem(_quantityIce, currentNumberOfIce);
        SpawnItem(_quantityHealth, currentNumberOfHealths);
        SpawnItem(_quantityDamage, currentNumberOfDamage);
    }

    void SpawnItem(QuatityItemToSpawn quatityItemToSpawn, int numberOfItems)
    {
        int amount = (_amountPoints % 2 == 0) ? _amountPoints : _amountPoints + 1;
        int maxItemsForLine = Mathf.RoundToInt(Mathf.Lerp(Mathf.RoundToInt((float)amount / 2f), 1f, _score.GetCurrentScoreProgress()));
        for (int i = 0; i < numberOfItems; i++)
        {
            CreateItem(quatityItemToSpawn.GetItemToSpawn(), Random.Range(1, maxItemsForLine), quatityItemToSpawn.ProbabilityToSpawn);
            transform.position += Vector3.up * _minDistanceToMoveY;
        }
    }

    private void CreateItem(AbstractItem item, int amount, float percentToSpawn)
    {
        List<Vector3> positions = new List<Vector3>(_positionsToSpawn);
        for (int i = 0; i < amount; i++)
        {
            if (Random.value > percentToSpawn)
            {
                continue;
            }
            int index = Random.Range(0, positions.Count);
            if (positions.Count == 0)
            {
                break;
            }
            Vector3 newPosition = transform.position + positions[index];
            Instantiate(item, newPosition, Quaternion.identity);
            positions.RemoveAt(index);
        }
    }

    private void OnDrawGizmos()
    {
        if (!_player)
        {
            return;
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 0.5f);
        Gizmos.color = Color.red;
        Vector3 playerPosition = transform.position;
        playerPosition.y = transform.position.y - _minDistanceToSpawn;
        Gizmos.DrawSphere(playerPosition, 0.5f);

        Gizmos.color = Color.cyan;
        _distanceBetweenSpawns = _distanceAvailableToSpawn / (_amountPoints - 1);
        Vector3 currnetPoint = transform.position;
        Gizmos.DrawLine(currnetPoint + (Vector3.left * (_distanceAvailableToSpawn / 2)), currnetPoint + (Vector3.right * (_distanceAvailableToSpawn / 2)));
        currnetPoint.x -= (_distanceAvailableToSpawn / 2);
        for (int i = 0; i < _amountPoints; i++)
        {
            Gizmos.DrawSphere(currnetPoint + Vector3.right * _distanceBetweenSpawns * i, 0.35f);
        }

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position + (Vector3.up * _minDistanceToMoveY), 0.5f);
    }
}
