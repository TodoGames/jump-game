﻿using System.Collections.Generic;
using UnityEngine;
using LevelEditor_Positions;
public class ItemsFactory : MonoBehaviour
{
    [SerializeField] private ObjectsToSpawnSO _objectsToSpawn;
    public GameObject GetNewObject(string idObjectToSpawn)
    {
        GameObject currentObject = null;
        if (_objectsToSpawn.GetObjectById(idObjectToSpawn,out currentObject))
        {
            return Instantiate(currentObject);
        }
        return null;
    }
}
