﻿using System.Collections.Generic;
using UnityEngine;
using LevelEditor_Positions;

public class PoolObjectSpawner : MonoBehaviour
{
    [SerializeField] private PoolObjectController _prefabPoolObjectController;
    [SerializeField] private ItemsFactory _itemsFactory;
    [SerializeField] private List<ObjectsPositionConfigurationSO> _objectsPositionConfigurations = new List<ObjectsPositionConfigurationSO>();
    [SerializeField] private int _numberOfRepit;
    private Dictionary<PoolObjectController, string> _poolObjectControllers = new Dictionary<PoolObjectController, string>();
    private void Awake()
    {
        for (int i = 0; i < _numberOfRepit; i++)
        {
            SpawnPoolObjectsConfigurations();
        }
    }

    private void SpawnPoolObjectsConfigurations()
    {
        foreach (var item in _objectsPositionConfigurations)
        {
            foreach (var configurations in item._currentConfiguration)
            {
                PoolObjectController currentPoolObject = Instantiate(_prefabPoolObjectController);
                foreach (var positions in configurations._kindOfObjectInPositions)
                {
                    currentPoolObject.SetNewGameObject(_itemsFactory.GetNewObject(positions._IdObject), positions._currentPosition);
                }
                _poolObjectControllers.Add(currentPoolObject, configurations._idKindOfConfiguration);
                currentPoolObject.transform.SetParent(transform);
                currentPoolObject.gameObject.SetActive(false);
            }
        }
    }

    public List<PoolObjectController> GetCurrentsPoolObjects(string typeOfPoolObject)
    {
        List<PoolObjectController> poolObjectControllers = new List<PoolObjectController>();
        foreach (var item in _poolObjectControllers)
        {
            if(item.Value == typeOfPoolObject && !item.Key.gameObject.activeSelf)
            {
                poolObjectControllers.Add(item.Key);
            }
        }
        return poolObjectControllers;
    }
}
