using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObjectController : MonoBehaviour
{
    private Dictionary<GameObject, Vector3> _currentObjectsSpawned = new Dictionary<GameObject, Vector3>();
    private Bounds _bounds = new Bounds();
    [SerializeField] private Vector2 _viewportPosition;
    [SerializeField] private float _offsetDistanceFromCamera;

    private void Update()
    {
        DisablePoolObjcet();
    }

    private void DisablePoolObjcet()
    {
        Vector3 cameraDownPosition = Camera.main.ViewportToWorldPoint(_viewportPosition);
        cameraDownPosition.z = 0;
        Vector3 currentPosition = transform.position;
        currentPosition.y += GetTopDistance() + _offsetDistanceFromCamera;
        if (Vector3.Distance(cameraDownPosition, currentPosition) < 0.5f)
        {
            gameObject.SetActive(false);
        }
    }

    public virtual void SetNewGameObject(GameObject newGameObject, Vector3 startPosiiton)
    {
        _currentObjectsSpawned.Add(newGameObject, startPosiiton);
        newGameObject.transform.SetParent(transform);
        newGameObject.transform.position = transform.position + startPosiiton;
        _bounds.Encapsulate(startPosiiton);
    }

    public virtual void OnEnable()
    {
        ActiveAllObejcts();
    }

    public virtual void ActiveAllObejcts()
    {
        RestartAllPositions();
        foreach (var item in _currentObjectsSpawned)
        {
            item.Key.SetActive(true);
        }
    }

    public virtual void RestartAllPositions()
    {
        foreach (var item in _currentObjectsSpawned)
        {
            item.Key.transform.position = transform.position + item.Value;
        }
    }

    public float GetDownDistance()
    {
        return Mathf.Abs(_bounds.center.y - _bounds.min.y);
    }

    public float GetTopDistance()
    {
        return Mathf.Abs(_bounds.center.y + _bounds.max.y);
    }
}
