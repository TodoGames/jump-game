﻿using System.Collections.Generic;
using UnityEngine;

public class PoolObjectPositioner : MonoBehaviour
{
    [SerializeField] private float _offsetBetweenPoolObjects;
    [SerializeField] private PoolObjectSpawner _poolObjectSpawner;
    [SerializeField] private string _currentTypeOfDifficulty;
    private List<PoolObjectController> _currentsPoolObjects = new List<PoolObjectController>();

    public void SetTypeOfDifficulty(string typeOfDifficulty)
    {
        _currentTypeOfDifficulty = typeOfDifficulty;
        ActiveNextPoolObjectController();
    }

    [ContextMenu("ActiveNextPoolObjectController")]
    public void ActiveNextPoolObjectController()
    {
        if(_currentsPoolObjects.Count == 0)
        {
            FillPoolObjectsControllers();
            Invoke("ActiveNextPoolObjectController", 0.5f);
            return;
        }
        int indexRandom = Random.Range(0, _currentsPoolObjects.Count);
        Vector3 newPosition = transform.position;
        newPosition.y += _currentsPoolObjects[indexRandom].GetDownDistance() + _offsetBetweenPoolObjects;
        _currentsPoolObjects[indexRandom].transform.position = newPosition;
        _currentsPoolObjects[indexRandom].gameObject.SetActive(true);

        newPosition.y += _currentsPoolObjects[indexRandom].GetTopDistance();
        transform.position = newPosition;
        _currentsPoolObjects.RemoveAt(indexRandom);
    }

    private void FillPoolObjectsControllers()
    {
        _currentsPoolObjects = _poolObjectSpawner.GetCurrentsPoolObjects(_currentTypeOfDifficulty);
    }
}