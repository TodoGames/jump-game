﻿using UnityEngine;

namespace LevelEditor_Positions
{
    public class ObjectInEditorPosition : MonoBehaviour
    {
        private LevelEditorController _positionEditor;
        [SerializeField] private string _IdObject;

        public void SetObjectsPositionEditor(LevelEditorController positionEditor)
        {
            _positionEditor = positionEditor;
        }

        public void SetKindOfObject(string IdObject, Color color)
        {
            _IdObject = IdObject;
            GetComponent<SpriteRenderer>().color = color;
        }

        private void OnMouseDown()
        {
            if (_positionEditor == null)
                return;

            if (_positionEditor._canDelete)
            {
                DeleteObject();
            }
        }

        private void OnMouseDrag()
        {
            if (_positionEditor == null || _positionEditor._canDelete)
                return;

            Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            newPosition.z = 0;
            transform.position = newPosition;
        }

        private void DeleteObject()
        {
            _positionEditor.DeleteObject(this);
        }

        public string GetIDObject()
        {
            return _IdObject;
        }
    }
}
