using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LevelEditor_Positions
{
    public class LevelEditorController : MonoBehaviour
    {
        [Header("Save Values")]
        [SerializeField] private List<ObjectInEditorPosition> _currentObjectsInPosition = new List<ObjectInEditorPosition>();
        [SerializeField] private ObjectInEditorPosition _objectInPositionToCreate;
        [SerializeField] private ObjectsPositionConfigurationSO _objectsPositionConfiguration;
        [SerializeField] private Text _textIndexConfiguration;
        private int _indexConfiguration;
        [Header("Buttons Items Creators")]
        [SerializeField] private ObjectsToSpawnSO _objectsToSpawn;
        [SerializeField] private Button _btnPrefabCreator;
        [SerializeField] private Transform _buttonsParent;
        [Header("Difficult Values")]
        [SerializeField] private Dropdown _dropdownDifficults;
        [SerializeField] private TypesOfDifficultySO _typesOfDifficultySO;
        [Header("Information")]
        [SerializeField] private Text _textInformation;
        [SerializeField] private float _timeInformation;
        Coroutine _coroutineTextInformation;
        [Header("Buttons Actions")]
        [SerializeField] private Button _btnUpdateSetting;
        [SerializeField] private Button _btnDeleteSetting;
        [SerializeField] private CanvasGroup _btnsActionsSettings;
        public bool _canDelete { get; set; }

        private void Start()
        {
            foreach (var item in _objectsToSpawn.GetListOfObjectsSettings())
            {
                Button newButtonToCreate = Instantiate(_btnPrefabCreator, _buttonsParent);
                newButtonToCreate.onClick.AddListener(() => 
                {
                    Vector2 newPosition = Camera.main.transform.position;
                    CreateObject(item._idObject, item._colorInEditor, newPosition); 
                });
                newButtonToCreate.GetComponentInChildren<Text>().text = item._idObject.ToString();
                newButtonToCreate.gameObject.SetActive(true);
            }

            _dropdownDifficults.ClearOptions();
            _dropdownDifficults.AddOptions(_typesOfDifficultySO.GetDifficultyNames());
            _dropdownDifficults.value = 0;

            _indexConfiguration = 0;
            LoadObjectsPositionsConfigurations();

            _dropdownDifficults.RefreshShownValue();
        }

        private void Update()
        {
            _textIndexConfiguration.text = "Configuracion N� " + _indexConfiguration;
            _textIndexConfiguration.color = IndexConfigurationInRange() ? Color.green : Color.red;

            if(_objectsPositionConfiguration._currentConfiguration.Count == 0)
            {
                _btnDeleteSetting.interactable = false;
                _btnUpdateSetting.interactable = false;
                _btnsActionsSettings.interactable = false;
            }
            else
            {
                _btnDeleteSetting.interactable = true;
                _btnUpdateSetting.interactable = true;
                _btnsActionsSettings.interactable = true;
            }
        }

        private bool IndexConfigurationInRange()
        {
            if (_objectsPositionConfiguration._currentConfiguration.Count == 0)
            {
                return false;
            }

            if (_indexConfiguration >= 0 && _indexConfiguration < _objectsPositionConfiguration._currentConfiguration.Count)
            {
                return true;
            }
            return false;
        }

        private ListOfObjectsPositions GetCurrentListOfObjects()
        {
            ListOfObjectsPositions listOfObjectsPositions = new ListOfObjectsPositions();
            listOfObjectsPositions._idKindOfConfiguration = _typesOfDifficultySO.GetNameByIndex(_dropdownDifficults.value);
            foreach (var item in _currentObjectsInPosition)
            {
                KindOfObjectInPosition kindOfObjectInPosition = new KindOfObjectInPosition();
                kindOfObjectInPosition._currentPosition = item.transform.position;
                kindOfObjectInPosition._IdObject = item.GetIDObject();
                listOfObjectsPositions._kindOfObjectInPositions.Add(kindOfObjectInPosition);
            }
            return listOfObjectsPositions;
        }

        public void SaveCurrentConfiguration()
        {
            CleanScreen();
            _objectsPositionConfiguration._currentConfiguration.Add(GetCurrentListOfObjects());            
            _indexConfiguration = _objectsPositionConfiguration._currentConfiguration.Count - 1;
            ActiveInformation("Creando nueva configuracion", Color.green);
        }

        public void UpdateCurrentConfiguration()
        {
            if (IndexConfigurationInRange())
            {
                _objectsPositionConfiguration._currentConfiguration[_indexConfiguration] = GetCurrentListOfObjects();
                ActiveInformation("Actualizacion Completa", Color.green);
            }
        }

        public void DeleteCurrentConfiguration()
        {
            if (_objectsPositionConfiguration._currentConfiguration.Count == 0)
            {
                ActiveInformation("No se Guardo alguna Configuracion", Color.red);
                CleanScreen();
                return;
            }

            if (IndexConfigurationInRange())
            {
                _objectsPositionConfiguration._currentConfiguration.RemoveAt(_indexConfiguration);
                ActiveInformation("Borrando Configuracion Actual", Color.red);
                OnClickPreviousConfiguration();
            }
        }

        public void CleanScreen()
        {
            foreach (var item in _currentObjectsInPosition)
            {
                Destroy(item.gameObject);
            }
            _currentObjectsInPosition.Clear();
        }

        public void OnClickNextConfiguration()
        {
            if (_objectsPositionConfiguration._currentConfiguration.Count == 0)
            {
                return;
            }
            _indexConfiguration++;
            if (_indexConfiguration >= _objectsPositionConfiguration._currentConfiguration.Count)
            {
                _indexConfiguration = 0;
            }
            LoadObjectsPositionsConfigurations();
        }

        public void OnClickPreviousConfiguration()
        {
            if (_objectsPositionConfiguration._currentConfiguration.Count == 0)
            {
                return;
            }
            _indexConfiguration--;
            if (_indexConfiguration < 0)
            {
                _indexConfiguration = _objectsPositionConfiguration._currentConfiguration.Count - 1;
            }
            LoadObjectsPositionsConfigurations();
        }

        public void LoadObjectsPositionsConfigurations()
        {
            if (_objectsPositionConfiguration._currentConfiguration.Count == 0)
            {
                return;
            }

            if (IndexConfigurationInRange())
            {
                CleanScreen();
                foreach (var item in _objectsPositionConfiguration._currentConfiguration[_indexConfiguration]._kindOfObjectInPositions)
                {
                    CreateObject(item._IdObject, _objectsToSpawn.GetColorById(item._IdObject), item._currentPosition);
                }
                _dropdownDifficults.value = _typesOfDifficultySO.GetIndexByName(_objectsPositionConfiguration._currentConfiguration[_indexConfiguration]._idKindOfConfiguration);
            }
            ActiveInformation("Cargando la configuracion", Color.yellow);
        }

        public void CreateObject(string idObject, Color kindOfColor, Vector3 position)
        {
            ObjectInEditorPosition objectInPosition = Instantiate(_objectInPositionToCreate, position, Quaternion.identity);
            _currentObjectsInPosition.Add(objectInPosition);
            objectInPosition.SetKindOfObject(idObject, kindOfColor);
            objectInPosition.SetObjectsPositionEditor(this);

            ActiveInformation("Objeto creado " + idObject, Color.green);
        }

        public void DeleteObject(ObjectInEditorPosition objectInPosition)
        {
            ActiveInformation("Objeto borrado " + objectInPosition.GetIDObject(), Color.red);
            if (_currentObjectsInPosition.Contains(objectInPosition))
            {
                _currentObjectsInPosition.Remove(objectInPosition);
                Destroy(objectInPosition.gameObject);
            }
        }

        private void ActiveInformation(string info, Color color)
        {
            if (_coroutineTextInformation != null)
            {
                StopCoroutine(_coroutineTextInformation);
            }

            _coroutineTextInformation = StartCoroutine(ShowInformation(info, color, _timeInformation));
        }

        IEnumerator ShowInformation(string info, Color color, float time)
        {
            _textInformation.gameObject.SetActive(true);
            _textInformation.text = info;
            _textInformation.color = color;
            yield return new WaitForSeconds(time);
            _textInformation.gameObject.SetActive(false);
        }
    }
}