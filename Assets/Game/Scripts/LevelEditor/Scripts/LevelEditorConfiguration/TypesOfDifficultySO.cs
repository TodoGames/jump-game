﻿using System.Collections.Generic;
using UnityEngine;

namespace LevelEditor_Positions
{
    [CreateAssetMenu(fileName = "TypesOfDifficulty", menuName = "LevelEditor_Configuration/Types Of Difficulty")]
    public class TypesOfDifficultySO : ScriptableObject
    {
        [SerializeField] private List<string> _difficultyNames = new List<string>();

        public List<string> GetDifficultyNames()
        {
            return _difficultyNames;
        }

        public string GetNameByIndex(int index)
        {
            if(index < 0 || index >= _difficultyNames.Count)
            {
                return "NONE";
            }
            return _difficultyNames[index];
        }

        public string GetNameByPercent(float amountPercent)
        {
            if(_difficultyNames.Count == 0)
            {
                return "NONE";
            }
            int currentIndex = Mathf.RoundToInt(Mathf.Lerp(0, _difficultyNames.Count-1, Mathf.Clamp01(amountPercent)));
            return _difficultyNames[currentIndex];
        }

        public int GetIndexByName(string name)
        {
            for (int i = 0; i < _difficultyNames.Count; i++)
            {
                if(_difficultyNames[i] == name)
                {
                    return i;
                }
            }
            return 0;
        }
    }
}