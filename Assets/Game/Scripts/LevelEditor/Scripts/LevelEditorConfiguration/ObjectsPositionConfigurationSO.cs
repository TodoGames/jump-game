﻿using System.Collections.Generic;
using UnityEngine;

namespace LevelEditor_Positions
{
    [CreateAssetMenu(fileName = "ObjectsPosition", menuName = "LevelEditor_Configuration/Objects Position")]
    public class ObjectsPositionConfigurationSO : ScriptableObject
    {
        public List<ListOfObjectsPositions> _currentConfiguration = new List<ListOfObjectsPositions>();
    }
}