﻿using System;
using System.Collections.Generic;
using UnityEditor;

namespace LevelEditor_Positions
{
    [Serializable]
    public class ListOfObjectsPositions
    {
        public string _idKindOfConfiguration;
        public List<KindOfObjectInPosition> _kindOfObjectInPositions = new List<KindOfObjectInPosition>();
    }
}
