﻿using System.Collections.Generic;
using UnityEngine;

namespace LevelEditor_Positions
{
    [CreateAssetMenu(fileName = "ObjectsToSpawn", menuName = "LevelEditor_Configuration/Objects To Spawn")]
    public class ObjectsToSpawnSO : ScriptableObject
    {
        [SerializeField] private List<ObjectsSettings> _objectRegisterToSpawns = new List<ObjectsSettings>();

        public virtual List<ObjectsSettings> GetListOfObjectsSettings()
        {
            return _objectRegisterToSpawns;
        }

        public virtual ObjectsSettings GetObjectSetting(string idObject)
        {
            foreach (var item in _objectRegisterToSpawns)
            {
                if (item._idObject == idObject)
                {
                    return item;
                }
            }
            return new ObjectsSettings();
        }

        public virtual bool GetObjectById(string idObject, out GameObject currentObject)
        {
            foreach (var item in _objectRegisterToSpawns)
            {
                if (item._idObject == idObject)
                {
                    currentObject = item._prefabToSpawn;
                    return true;
                }
            }
            currentObject = null;
            return false;
        }

        public virtual Color GetColorById(string idObject)
        {
            foreach (var item in _objectRegisterToSpawns)
            {
                if (item._idObject == idObject)
                {
                    return item._colorInEditor;
                }
            }
            return Color.white;
        }

        [System.Serializable]
        public struct ObjectsSettings
        {
            public string _idObject;
            public GameObject _prefabToSpawn;
            public Color _colorInEditor;
        }
    }
}