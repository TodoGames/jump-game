using UnityEngine;
using System;

namespace LevelEditor_Positions
{
    [Serializable]
    public class KindOfObjectInPosition
    {
        public string _IdObject;
        public Vector3 _currentPosition;
    }
}
