using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLevelEditor : MonoBehaviour
{
    [SerializeField] private Camera _zoomCamera;
    [Header("Zoom")]
    [SerializeField] private float _speedZoom;
    [SerializeField] private RangeValues _valuesOrthographic;
    [SerializeField] private RangeValues _valuesFieldOfView;
    [Header("Movement")]
    [SerializeField] private float _speedMovement;
    private Vector2 _positionStart;
    private Vector2 _positionEnd;
    private float _currentDistance;

    public float ZoomValue { get; set; }
    public float HorizontalDirection { get; set; }
    public float VerticalDirection { get; set; }
    private void Start()
    {
        _zoomCamera = Camera.main;
    }

    private void Update()
    {
        //TouchController();
        //MouseController();

        SetZoomCamera(ZoomValue);
        MoveCamera(new Vector2(HorizontalDirection, VerticalDirection));
    }

    private void TouchController()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                _positionStart = Input.GetTouch(0).position;
            }
        }

        if (Input.touchCount > 1)
        {
            if (Input.GetTouch(1).phase == TouchPhase.Began)
            {
                _positionEnd = Input.GetTouch(1).position;
                _currentDistance = Vector2.Distance(_positionEnd, _positionStart);
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                float moveDistance = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
                SetZoomCamera(Mathf.Sign(moveDistance - _currentDistance));
            }
        }
    }

    private void MouseController()
    {
        ZoomValue=Input.GetAxis("Mouse ScrollWheel") * -1;

        HorizontalDirection = Input.GetAxis("Horizontal");
        VerticalDirection= Input.GetAxis("Vertical");
    }

    public void SetZoomCamera(float value)
    {
        float newValue = value * _speedZoom * Time.deltaTime;
        if (_zoomCamera.orthographic)
        {
            newValue += _zoomCamera.orthographicSize;
            _zoomCamera.orthographicSize = _valuesOrthographic.GetCurrentValue(newValue);
        }
        else
        {
            newValue += _zoomCamera.fieldOfView;
            _zoomCamera.fieldOfView = _valuesFieldOfView.GetCurrentValue(newValue);
        }
    }

    public void MoveCamera(Vector2 direction)
    {
        _zoomCamera.transform.Translate(direction * _speedMovement * Time.deltaTime);
    }

    public void RecenterCamera()
    {
        _zoomCamera.transform.position = new Vector3(0f, 0f, _zoomCamera.transform.position.z);
    }

    [System.Serializable]
    public struct RangeValues
    {
        [SerializeField] private float _minValue;
        [SerializeField] private float _maxValue;

        public float GetCurrentValue(float value)
        {
            return Mathf.Clamp(value, _minValue, _maxValue);
        }
    }
}
