using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPhysics : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private float _maxVerticalVelocity;
    [SerializeField] private float _impulseForce;
    [SerializeField] private string _tagCollision;
    void Start()
    {
        
    }

    void Update()
    {
        Vector2 currentVelocity = _rigidbody2D.velocity;
        currentVelocity.y = Mathf.Clamp(currentVelocity.y, -_maxVerticalVelocity, _maxVerticalVelocity * 2);
        _rigidbody2D.velocity = currentVelocity;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(_tagCollision))
        {
            if(collision.GetContact(0).point.y < transform.position.y)
            {
                Vector2 impulseDirection = collision.GetContact(0).normal;
                _rigidbody2D.AddForce(impulseDirection * _impulseForce, ForceMode2D.Impulse);
            }
            //if(impulseDirection.y <= 0.95f)
            //{
            //    _rigidbody2D.AddForce(impulseDirection * _impulseForce, ForceMode2D.Impulse);
            //}
            //else
            //{
            //    Vector3 newDirection = _rigidbody2D.velocity.normalized;
            //    newDirection.y = 1;
            //    _rigidbody2D.AddForce(newDirection * _impulseForce, ForceMode2D.Impulse);
            //}
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IJumpRigidbody2D jumpCollision = collision.gameObject.GetComponent<IJumpRigidbody2D>();
        if (jumpCollision != null)
        {
            jumpCollision.DoJump(_rigidbody2D);
        }
    }
}
