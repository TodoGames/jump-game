using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputTouchToPlatforms : InputPlayerTouch
{
    public override void CheckInteractionTouch(Touch touchInteraction)
    {
        Vector3 currentPosition = touchInteraction.position;
        int fingerID = touchInteraction.fingerId;
        if (!CheckInteractionWithUI(currentPosition, fingerID))
        {
            CheckInterationWithPlatform(currentPosition, fingerID);
        }
    }

    private bool CheckInteractionWithUI(Vector3 currentPosition, int fingerID)
    {
        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = currentPosition;

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerEventData, raycastResults);
        foreach (var item in raycastResults)
        {
            var createPlatformOnClik = item.gameObject.GetComponent<CreatePlatformOnClick>();
            if (createPlatformOnClik != null)
            {
                Vector3 positionToSpawn = Camera.main.ScreenToWorldPoint(Input.GetTouch(fingerID).position);
                positionToSpawn.z = 0;
                createPlatformOnClik.SpawnPlatform(fingerID, positionToSpawn);
                return true;
            }

            var ignoreCanvasUI = item.gameObject.GetComponent<IgnoreCanvasToGame>();
            if(ignoreCanvasUI != null)
            {
                return true;
            }
        }
        return false;
    }

    private bool CheckInterationWithPlatform(Vector3 currentPosition, int fingerID)
    {
        Vector3 originalPosition = Camera.main.ScreenToWorldPoint(currentPosition);
        originalPosition.z = 0;
        RaycastHit2D _raycastHit2D = Physics2D.Raycast(originalPosition, Vector3.zero, _distanceRaycast, _layerMaskToTouch);
        if (_raycastHit2D)
        {
            var mobilePlatform = _raycastHit2D.transform.GetComponent<MobilePlatform>();
            if (mobilePlatform != null)
            {
                mobilePlatform.OnClickMouseDown(fingerID);
                return true;
            }
        }
        return false;
    }
}
