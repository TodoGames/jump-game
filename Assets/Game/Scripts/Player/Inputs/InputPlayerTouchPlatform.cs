﻿using UnityEngine;

public class InputPlayerTouchPlatform : InputPlayerTouch
{
    private PlatformPlayerController _platformPlayerController;
    private Touch _currentTouch;

    protected override void Update()
    {
        base.Update();
        if(_platformPlayerController != null)
        {
            if(_currentTouch.fingerId >= Input.touchCount)
            {
                return;
            }
            Vector3 originalPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(_currentTouch.fingerId).position);
            originalPosition.z = 0;
            _platformPlayerController.SetNewPosition(originalPosition);
        }
    }

    public override void CheckInteractionTouch(Touch touchInteraction)
    {
        base.CheckInteractionTouch(touchInteraction);
        Vector3 originalPosition = Camera.main.ScreenToWorldPoint(touchInteraction.position);
        originalPosition.z = 0;
        RaycastHit2D _raycastHit2D = Physics2D.Raycast(originalPosition, Vector3.zero, _distanceRaycast, _layerMaskToTouch);
        if (_raycastHit2D)
        {
            _platformPlayerController = _raycastHit2D.transform.GetComponent<PlatformPlayerController>();
            if(_platformPlayerController != null)
            {
                _currentTouch = touchInteraction;
                return;
            }

            var mobilePlatform = _raycastHit2D.transform.GetComponent<MobilePlatform>();
            if (mobilePlatform != null)
            {
                mobilePlatform.OnClickMouseDown(touchInteraction.fingerId);
                return;
            }
        }
    }

    public override void EndInteractionTouch(Touch touchInteraction)
    {
        base.EndInteractionTouch(touchInteraction);
        if(touchInteraction.fingerId == _currentTouch.fingerId)
        {
            _platformPlayerController = null;
        }
    }
}