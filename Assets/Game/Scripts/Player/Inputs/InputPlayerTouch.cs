﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputPlayerTouch : MonoBehaviour
{
    [SerializeField] protected LayerMask _layerMaskToTouch;
    [SerializeField] protected float _distanceRaycast;
    [SerializeField] protected GameObject _touchPointPrefab;
    [SerializeField] protected int _maxPoints;
    protected List<TouchPoint> _currentTouchPoints = new List<TouchPoint>();

    protected virtual void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject newTouchPointObject = Instantiate(_touchPointPrefab);
            TouchPoint newTouchPoint = new TouchPoint(newTouchPointObject);
            _currentTouchPoints.Add(newTouchPoint);
        }
    }

    protected virtual void Update()
    {
        if (Input.touchCount > 0)
        {
            RaycastCheckToInteraction();
        }
    }

    private void RaycastCheckToInteraction()
    {
        Touch[] currentTouchs = Input.touches;
        int maxTouchs = _maxPoints;
        if(currentTouchs.Length <= _maxPoints)
        {
            maxTouchs = currentTouchs.Length;
        }
        for (int i = 0; i < maxTouchs; i++)
        {
            if (currentTouchs[i].phase == TouchPhase.Began)
            {
                CheckInteractionTouch(currentTouchs[i]);
                _currentTouchPoints[currentTouchs[i].fingerId].ActivePoint();
            }
            Vector3 touchPosition = Camera.main.ScreenToWorldPoint(currentTouchs[i].position);
            touchPosition.z = 0;
            _currentTouchPoints[currentTouchs[i].fingerId].SetPointPosition(touchPosition);

            if (currentTouchs[i].phase == TouchPhase.Ended)
            {
                _currentTouchPoints[currentTouchs[i].fingerId].DesactivePoint();
                EndInteractionTouch(currentTouchs[i]);
            }
        }
    }

    public virtual void CheckInteractionTouch(Touch touchInteraction)
    {
        Debug.Log("Realizando Touch: " + touchInteraction.fingerId);
    }

    public virtual void EndInteractionTouch(Touch touchInteraction)
    {
        Debug.Log("End Touch: " + touchInteraction.fingerId);
    }

    public struct TouchPoint
    {
        public GameObject _point;
        public TouchPoint(GameObject point)
        {
            _point = point;
            DesactivePoint();
        }
        public void ActivePoint()
        {
            _point.SetActive(true);
        }

        public void SetPointPosition(Vector2 position)
        {
            _point.transform.position = position;
        }

        public void DesactivePoint()
        {
            _point.SetActive(false);
        }
    }
}
