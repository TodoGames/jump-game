﻿using UnityEngine;

public class PlayerInitializer : MonoBehaviour
{
    [SerializeField] private PlayerConfigurationSO playerConfigurationSO;
    [SerializeField] private AbstractHealth healthOfPlayer;
    [SerializeField] private GravityLimiter gravityLimiter;
    private void Awake()
    {
        healthOfPlayer.SetCurrentHealth(playerConfigurationSO.initialHealth);
        if(gravityLimiter) gravityLimiter.Configuration(playerConfigurationSO.maxFall, playerConfigurationSO.maxJump);
    }
}