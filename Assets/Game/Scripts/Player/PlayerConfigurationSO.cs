﻿using UnityEngine;

[CreateAssetMenu(fileName = "Player Configuration", menuName = "Configurations/PlayerConfig")]
public class PlayerConfigurationSO : ScriptableObject
{
    public float maxJump;
    public float maxFall;
    public int initialHealth;
}