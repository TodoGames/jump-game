using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private string _tagPlatformCollision;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var jumpIntoCollision = collision.gameObject.GetComponent<IJumpRigidbody2D>();
        if (jumpIntoCollision != null)
        {
            jumpIntoCollision.DoJump(_rigidbody2D);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(_tagPlatformCollision))
        {
            var damageHealth = collision.gameObject.GetComponent<IDamageHealth>();
            if (damageHealth != null)
            {
                damageHealth.DecrementHealth(1);
            }
        }
    }
}
