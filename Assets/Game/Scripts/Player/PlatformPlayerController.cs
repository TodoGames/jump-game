using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPlayerController : MonoBehaviour
{
    public enum KindMovement
    {
        Horizontal,
        Vertical,
    }

    [SerializeField] private Transform _minLimitPosition;
    [SerializeField] private Transform _maxLimitPosition;
    [SerializeField] private KindMovement _kindMovement;
    [SerializeField] private BoxCollider2D _boxColliderToTouch;
    [SerializeField] private Vector2 _offsetTouch;
    [SerializeField] private PlatformController _defaultPlatformController;
    private PlatformController _currentPlatformController;
    private void Start()
    {
        InitPlatformPlayer();
        SetDefaultPlatform();
        UpdateColliderTouch();
    }

    private void InitPlatformPlayer()
    {
        Vector3 originalPosition = transform.position;
        originalPosition.y = _minLimitPosition.position.y;
        transform.position = originalPosition;
    }

    public void SetNewPosition(Vector3 worldPosition)
    {
        worldPosition.y = transform.position.y;
        if (worldPosition.x > _maxLimitPosition.position.x)
        {
            worldPosition.x = _maxLimitPosition.position.x;
        }

        if(worldPosition.x < _minLimitPosition.position.x)
        {
            worldPosition.x = _minLimitPosition.position.x;
        }

        transform.position = worldPosition;
    }

    public void SetDefaultPlatform()
    {
        Quaternion quaternionSpawn = _defaultPlatformController.transform.rotation;
        SetCurrentPlatformController(Instantiate(_defaultPlatformController, transform.position, quaternionSpawn));
    }

    public void SetCurrentPlatformController(PlatformController platformController)
    {
        if(_currentPlatformController != null)
        {
            Destroy(_currentPlatformController.gameObject);
        }
        _currentPlatformController = platformController;
        _currentPlatformController.transform.SetParent(transform);
        UpdateColliderTouch();
    }

    public void UpdateColliderTouch()
    {
        if(_currentPlatformController != null)
        {
            _boxColliderToTouch.size = (Vector2)_currentPlatformController.transform.localScale + _offsetTouch;
        }
    }
}
